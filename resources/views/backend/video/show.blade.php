@extends('layouts.backend')
@section('title','Video View page')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header" xmlns="http://www.w3.org/1999/html">
        <h1>
            Video Management
            <a href="{{route('video.create')}}" class="btn btn-success">
                <i class="fa fa-plus"></i>
                Create
            </a>

            <a href="{{route('video.index')}}" class="btn btn-info">
                <i class="fa fa-list"></i>
                List
            </a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Video</a></li>
            <li class="active">Show page</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Title</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                @include('includes.flash')
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>URL</th>
                        <td>{{$data['videos']->url}}</td>
                    </tr>
                    <tr>
                        <th>Status</th>
                        <td>
                            @if($data['videos']->status == 1)
                                <span class="label label-success">Active</span>
                            @else
                                <span class="label label-danger">De Active</span>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>Created By</th>
                        <td>{{App\User::find($data['videos']->created_by)->name}}
                        </td>

                    </tr>
                    <tr>
                        <th>Created Date</th>
                        <td>{{$data['videos']->created_at}}
                        </td>
                    </tr>
                    <tr>
                        <th>Updated Date</th>
                        <td>{{$data['videos']->updated_at}}
                        </td>
                    </tr>
                    <tr>
                        <th>Updated By</th>
                        <td>@if(!empty($data['videos']->updated_by))
                                {{App\User::find($data['videos']->updated_by)->name}}
                            @endif
                        </td>
                    </tr>
                    </thead>
                </table>
                {{--<div class="row">--}}
                    {{--{{$data['video']->image}}--}}
                    {{--<div class="col-md-3">--}}
                        {{--<div class="img-container">--}}
                            {{--<button class="btn btn-danger btn-close">X</button>--}}
                            {{--<img src="{{asset('images/video/' .$data['videos']->feature_image)}}" alt="" height="100" width="100">--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                Footer
            </div>
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection