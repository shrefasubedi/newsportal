<?php

namespace App\Http\Controllers\Frontend;

use App\Model\Advertisement;
use App\Model\Author;
use App\Model\Category;
use App\Model\Comment;
use App\Model\Configuration;
use App\Model\Contact;
use App\Model\Gallery;
use App\Model\News;
use App\Model\NewsSlider;
use App\Model\NewsType;
use App\Model\Page;
use App\Model\Tag;
use App\Model\Video;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class FrontController extends Controller
{
    function index()
    {
        $data = [];
        $data['configurations'] = Configuration::first();
        $data['pages'] = Page::first();
        $data['categories'] = Category::where('status', 1)->orderby('rank')->get();
        $data['advertisements'] = Advertisement::where('status', 1)->orderby('rank')->get();
        $data['top_categories'] = Category::where('status',1)->orderby('rank')->limit(1)->get();
        $data['news'] = News::where('status', 1)->limit(2)->get();
        $data['news_sliders'] = NewsSlider::where('status', 1)->limit(2)->get();
        $data['latest_news'] = News::where('status', 1)->limit(4)->get();
        $data['breaking_news']=News::where('status',1)->limit(4)->get();
        $breaking_news = NewsType::where('name','Breaking News')->get();
        $breaking_news[0]->news()->limit(4)->get();
        $data['galleries']=Gallery::where('status',1)->limit(6)->orderby('created_at','desc')->get();

        $popular_news = NewsType::where('name','Popular News')->get();
        $data['popular_news']=News::where('status',1)->whereHas('newstypes', function ($query) {
            $query->where('name', '=', 'Popular News');
        })->limit(2)->orderby('created_at','desc')->get();
        $popular_news[0]->news()->limit(3)->get();


        $top_news = NewsType::where('name','Top News')->get();
        $data['top_news']=News::where('status',1)->whereHas('newstypes', function ($query) {
            $query->where('name', '=', 'Top News');
        })->limit(2)->orderby('created_at','desc')->limit(2)->paginate(3);
        $top_news[0]->news()->limit(3)->get();
        $data['videos']=Video::where('status',1)->get();

//        $categoryNews = NewsType::where('name','Feature News')->get();
//        $data['categoryNews']=News::where('status',1)->whereHas('newstypes', function ($query) {
//            $query->where('name', '=', 'Feature News');
//        })->limit(2)->orderby('created_at','desc')->get();
//        $top_news[0]->news()->limit(3)->get();

        $data['feature_news'] = News::where('status',1)->limit(3)->get();
        $feature_news = NewsType::where('name','Feature News')->get();
        $feature_news[0]->news()->limit(3)->get();

//        $data['categoryNews']=News::where('status',1)->limit(2)->orderby('created_at','desc')->get();
//        $categoryNews = NewsType::where('name','Feature News')->get();
//        $categoryNews[0]->news()->limit(2)->get();

        $data['categories'] = News::where('status',1)->get();
        $data['categories'] = Category::where('status',1)->get();

        return view('frontend.front.index', compact('data'));
    }
    function  category($slug){
        $data = [];
        $data['configurations'] = Configuration::first();
        $data['pages'] = Page::first();
        $data['categories'] = Category::where('status',1)->orderby('rank')->get();
        $data['category'] = Category::where('slug',$slug)->get();
        $data['galleries']=Gallery::where('status',1)->limit(6)->orderby('created_at','desc')->get();
        $data['popular_news']=News::where('status',1)->limit(2)->orderby('created_at','desc')->get();
        $popular_news = NewsType::where('name','Breaking News')->get();
        $popular_news[0]->news()->limit(3)->get();
        $data['top_news'] = News::where('status',1)->limit(2)->get();
        $top_news = NewsType::where('name','Top News')->get();
        $top_news[0]->news()->limit(3)->get();

        return view('frontend.front.category',compact('data'));

    }
    function  categorydetail($slug){
        $data = [];
        $data['configurations'] = Configuration::first();
        $data['pages'] = Page::first();
        $data['news'] = News::where('slug',$slug)->get();
        $data['category'] = Category::where('slug',$slug)->get();
        $data['categories'] = Category::where('status',1)->orderby('rank')->get();
        $data['galleries']=Gallery::where('status',1)->limit(6)->orderby('created_at','desc')->get();
        $data['advertisements'] = Advertisement::where('status', 1)->orderby('rank')->get();
        $data['breaking_news'] = News::where('status',1)->limit(4)->get();
        $breaking_news=NewsType::where('name','Breaking News')->get();
        $breaking_news[0]->news()->limit(4)->get();
//        $data['popular_news'] = News::where('status',1)->limit(3)->get();
        $data['popular_news']=News::where('status',1)->limit(3)->orderby('created_at','desc')->get();
        $popular_news = NewsType::where('name','Popular News')->get();
        $popular_news[0]->news()->limit(3)->get();

        $data['news'] = News::where('status',1)->where('category_id',$data['category'][0]->id)->paginate(3);

        $data['top_news'] = News::where('status',1)->limit(3)->get();
        $top_news = NewsType::where('name','Top News')->get();
        $top_news[0]->news()->limit(3)->get();


        $data['latest_news'] = News::where('status',1)->where('category_id',$data['category'][0]->id)->limit(4)->get();
        $data['feature_news'] = News::where('status',1)->where('category_id',$data['category'][0]->id)->paginate(5);
        $data['categorydetail'] = News::where('slug',$slug)->get();
        $data['categoryNews'] = $data['category'][0]->news()->get();

//        $data['news']['author'] = $data['categorydetail'][0]->author()->first();
        return view('frontend.front.categorydetail',compact('data'));

    }
    function  postdetail($slug ){
        $data = [];
        $data['configurations'] = Configuration::first();
        $data['pages'] = Page::first();
        $data['author'] = Author::where('status',1)->get();
        $data['comments'] = Comment::where('status',1)->limit(2)->get();
//        $data['comments'] = Comment::where('news_id',$data['comments'][0]->id)->get();
//        dd($data['comments']);
        $data['news'] = News::where('slug',$slug)->get();

        $data['category'] = Category::where('slug',$slug)->get();
        $data['categories'] = Category::where('status',1)->orderby('rank')->get();

        $data['category'] = News::where('slug',$slug)->get();

        $data['advertisements'] = Advertisement::where('status', 1)->orderby('rank')->get();
        $data['galleries']=Gallery::where('status',1)->limit(6)->orderby('created_at','desc')->get();
        $data['breaking_news'] = News::where('slug',$slug)->get();
        $data['breaking_news'] = News::where('status',1)->limit(4)->get();
        $breaking_news=NewsType::where('name','Breaking News')->get();
        $breaking_news[0]->news()->limit(4)->get();

        $data['popular_news']=News::where('status',1)->limit(2)->orderby('created_at','desc')->get();
        $popular_news = NewsType::where('name','Breaking News')->get();
        $popular_news[0]->news()->limit(3)->get();


        $data['top_news'] = News::where('status',1)->limit(2)->get();
        $top_news = NewsType::where('name','Top News')->get();
        $top_news[0]->news()->limit(3)->get();

//        $news = News::where('slug',$slug)->with('tags')->get();
//
//        $data['comments'] = Comment::where('news_id',$data['comments'][0]->id)->paginate(3);

//        $data['news'] = Tag::where('status',1)->limit(3)->get();
        $data['latest_news']=News::where('slug',$slug)->get();
        $data['latest_news'] = News::where('status',1)->limit(4)->get();
        $data['categorydetail'] = News::where('slug',$slug)->get();
        $data['postdetail'] = News::where('slug',$slug)->get();
        $data['news']['author'] = $data['postdetail'][0]->author()->first();
        $data['news']['category'] = $data['postdetail'][0]->category()->first();
//        $data['news']['comment'] = $data['postdetail'][0]->comment()->first();
////        $data['postdetail'][0]->author()->get();
////        $data['postdetail'][0]->author()->get();
        return view('frontend.front.postdetail',compact('data'));

    }

    function  contact(){
        $data = [];
        $data['configurations'] = Configuration::first();
        $data['pages'] = Page::first();
        $data['categories'] = Category::where('status',1)->orderby('rank')->get();
//        $data['category'] = Category::where('slug',$slug)->get();
        $data['galleries']=Gallery::where('status',1)->limit(6)->orderby('created_at','desc')->get();
        $data['advertisements'] = Advertisement::where('status', 1)->orderby('rank')->get();
        $data['breaking_news'] = News::where('status',1)->limit(4)->get();

        $data['popular_news']=News::where('status',1)->limit(3)->orderby('created_at','desc')->get();
        $popular_news = NewsType::where('name','Breaking News')->get();
        $popular_news[0]->news()->limit(3)->get();


        $data['top_news'] = News::where('status',1)->limit(3)->get();
        $top_news = NewsType::where('name','Top News')->get();
        $top_news[0]->news()->limit(3)->get();

        $data['contacts'] = Contact::all();
        return view('frontend.front.contact',compact('data'));
    }

    function popular_news($slug){
        $data = [];
        $data['configurations'] = Configuration::first();
        $data['pages'] = Page::first();
//        $data['popular_news']=News::where('status',1)->limit(3)->orderby('created_at','desc')->paginate(5);
//        $popular_news = NewsType::where('name','Popular News')->get();
//        $popular_news[0]->news()->limit(3)->paginate(2);
//
        $popular_news = NewsType::where('name','Popular News')->get();
        $data['popular_news']=News::where('status',1)->whereHas('newstypes', function ($query) {
            $query->where('name', '=', 'Popular News');
        })->limit(2)->orderby('created_at','desc')->limit(2)->paginate(3);
        $popular_news[0]->news()->limit(2)->get();


        $data['advertisements'] = Advertisement::where('status', 1)->orderby('rank')->get();
        $data['categories'] = Category::where('status',1)->orderby('rank')->get();
        $data['breaking_news'] = News::where('status',1)->limit(4)->get();
        $data['galleries']=Gallery::where('status',1)->limit(6)->orderby('created_at','desc')->get();
        $data['top_news'] = News::where('status',1)->limit(3)->get();
        $top_news = NewsType::where('name','Top News')->get();
        $top_news[0]->news()->limit(3)->get();


        return view('frontend.front.popular_news',compact('data'));
    }

    function top_news(){
        $data = [];
        $data['configurations'] = Configuration::first();
        $data['pages'] = Page::first();
        $top_news = NewsType::where('name','Top News')->get();
        $data['top_news']=News::where('status',1)->whereHas('newstypes', function ($query) {
            $query->where('name', '=', 'Top News');
        })->limit(2)->orderby('created_at','desc')->limit(2)->paginate(3);
        $top_news[0]->news()->limit(2)->get();
        $popular_news = NewsType::where('name','Popular News')->get();
        $data['popular_news']=News::where('status',1)->whereHas('newstypes', function ($query) {
            $query->where('name', '=', 'Popular News');
        })->limit(3)->orderby('created_at','desc')->limit(3)->get();
        $popular_news[0]->news()->limit(3)->get();
        $data['advertisements'] = Advertisement::where('status', 1)->orderby('rank')->get();
        $data['categories'] = Category::where('status',1)->orderby('rank')->get();
        $data['breaking_news'] = News::where('status',1)->limit(4)->get();
        $data['galleries']=Gallery::where('status',1)->limit(6)->orderby('created_at','desc')->get();
        return view('frontend.front.top_news',compact('data'));
    }


    public function store(Request $request)
    {
        //dd($request->all());
        //add login user id to request object
        $request->request->add(['created_by' => Auth::user()->id]);


        //  dd($request->all());
        //insert into category table using Category model
        $contact = Contact::create($request->all());


        if ($contact) {
            $request->session()->flash('success_message', 'Contact Created Successfully');
            return redirect()->route('frontend.contact');
        } else {
            $request->session()->flash('error_message', 'Contact Creation Failed');
//            return redirect()->route('page.create');
        }
    }
//    public function comment(Request $request){
//        $request->request->add(['created_by' => Auth::user()->id]);
//        $comment = Comment::create($request->all());
//        if ($comment){
//            $request->session()->flash('success_message', 'Comment created successfully');
//            return redirect()->route('frontend.postdetail');
//        } else{
//            $request->session()->flash('error_message', 'Cmment Creation Failed');
//        }
//    }

    public function search(Request $request )
    {
        $data = [];
        $data['configurations'] = Configuration::first();
        $data['pages'] = Page::first();
        $data['categories'] = Category::where('status',1)->orderby('rank')->get();
//        $data['category'] = Category::where('slug',$slug)->get();
        $data['galleries']=Gallery::where('status',1)->limit(6)->orderby('created_at','desc')->get();



        $data['advertisements'] = Advertisement::where('status', 1)->orderby('rank')->get();
        $data['top_categories'] = Category::where('status',1)->orderby('rank')->limit(1)->get();
        $data['news'] = News::where('status', 1)->limit(2)->get();
        $data['news_sliders'] = NewsSlider::where('status', 1)->limit(2)->get();
        $data['latest_news'] = News::where('status', 1)->limit(4)->get();
        $data['breaking_news']=News::where('status',1)->limit(4)->get();
        $breaking_news = NewsType::where('name','Breaking News')->get();
        $breaking_news[0]->news()->limit(4)->get();

        $data['popular_news']=News::where('status',1)->limit(2)->orderby('created_at','desc')->get();
        $popular_news = NewsType::where('name','Breaking News')->get();
        $popular_news[0]->news()->limit(3)->get();

        $data['top_news'] = News::where('status',1)->limit(2)->get();
        $top_news = NewsType::where('name','Top News')->get();
        $top_news[0]->news()->limit(3)->get();

        $search = $request->get('search');
        $data['news'] = News::where('title', 'LIKE', '%'.$search.'%')->get();
        return view('frontend.front.search',compact('data'));

    }

    function comment(Request $request)
    {
        $comment = Comment::create($request->all());
        if ($comment) {
            $request->session()->flash('success_message', 'Comment Created Successfully');
            return redirect()->back();
        } else {
            $request->session()->flash('error_message', 'Comment Creation Failed');
//            return redirect()->route('configuration.create');
        }

}



}
