<div class="form-group">
    <label for="page_id">Page_id</label>
    {!! Form::select('page_id', $data['pages'],null,['class' => 'form-control']); !!}

</div>

<div class="form-group">
    {!!  Form::label('title', 'Title'); !!}
    {!! Form::text('title', null,['class' => 'form-control','id' => 'title','placeholder' => 'enter title']); !!}
    @include('includes.single_field_validation',['field'=>'title'])
</div>

<div class="form-group">
    {!!  Form::label('rank', 'Rank'); !!}
    {!! Form::number('rank', null,['class' => 'form-control','id' => 'rank']); !!}

    @include('includes.single_field_validation',['field'=>'rank'])
</div>
<div class="form-group">
    {!!  Form::label('expired_at', 'Expired_At'); !!}
    {!! Form::date('expired_at', null,['class' => 'form-control','id' => 'expired_at']); !!}

    @include('includes.single_field_validation',['field'=>'expired_at'])
</div>
<div class="form-group">
    {!!  Form::label('photo', ' Image'); !!}
    {!! Form::file('photo'); !!}

    @include('includes.single_field_validation',['field'=>'photo'])
</div>

<div class="form-group">
    {!!  Form::label('description', ' Description'); !!}
    {!! Form::textarea('description', null,['class' => 'form-control','id' => 'description']); !!}

    @include('includes.single_field_validation',['field'=>'description'])
</div>

<div class="form-group">
    {!!  Form::label('status', 'Status'); !!}
    {!! Form::radio('status', '1') !!} Active
    {!! Form::radio('status', '0',true) !!} De Active
</div>