@extends('layouts.frontend')
@section('content')
    <!-- Post Section Start -->
    <div class="post-section section mt-50">
        <div class="container">

            <!-- Feature Post Row Start -->
            <div class="row">

                <div class="col-lg-8 col-12 mb-50">

                    <!-- Post Block Wrapper Start -->
                    <div class="post-block-wrapper">

                        <!-- Post Block Body Start -->
                        <div class="body">

                            <!-- Post Start -->
                            @foreach ($data['popular_news'] as $popular_news)
                                <div class="post fashion-post post-default-list post-separator-border">
                                    <div class="post-wrap">

                                        <!-- Image -->
                                        <a class="image" href="{{route('frontend.postdetail',$popular_news->slug)}}"><img src="{{asset('images/news/' . $popular_news->feature_image)}}" alt="feature_image"></a>

                                        <!-- Content -->
                                        <div class="content">

                                            <!-- Title -->
                                            <h4 class="title"><a href="{{route('frontend.postdetail',$popular_news->slug)}}">{{$popular_news->meta_keyword}}</a></h4>

                                            <!-- Meta -->
                                            <div class="meta fix">
                                                <a href="#" class="meta-item author"><i class="fa fa-user"></i>Sathi Bhuiyan</a>
                                                <span class="meta-item date"><i class="fa fa-clock-o"></i>10 March 2017</span>
                                            </div>

                                            <!-- Description -->
                                            <p>{{$popular_news->short_description}}</p>                                        <!-- Read More -->
                                            <a href="{{route('frontend.postdetail',$popular_news->slug)}}" class="read-more">continue reading</a>

                                        </div>

                                    </div>
                                </div><!-- Post End -->

                            @endforeach

                            <div class="pages text-right">
                                {{$data['popular_news']->links()}}
                            </div>

                        </div><!-- Post Block Body End -->

                        {{--<!-- Post Block Body Start -->--}}
                        {{--<div class="body">--}}

                        {{--<!-- Post Start -->--}}
                        {{--@foreach ($data['top_news'] as $top_news)--}}
                        {{--<div class="post fashion-post post-default-list post-separator-border">--}}
                        {{--<div class="post-wrap">--}}

                        {{--<!-- Image -->--}}
                        {{--<a class="image" href="{{route('frontend.postdetail',$top_news->slug)}}"><img src="{{asset('images/news/' . $top_news->feature_image)}}" alt="feature_image"></a>--}}

                        {{--<!-- Content -->--}}
                        {{--<div class="content">--}}

                        {{--<!-- Title -->--}}
                        {{--<h4 class="title"><a href="{{route('frontend.postdetail',$top_news->slug)}}">{{$top_news->meta_keyword}}</a></h4>--}}

                        {{--<!-- Meta -->--}}
                        {{--<div class="meta fix">--}}
                        {{--<a href="#" class="meta-item author"><i class="fa fa-user"></i>Sathi Bhuiyan</a>--}}
                        {{--<span class="meta-item date"><i class="fa fa-clock-o"></i>10 March 2017</span>--}}
                        {{--</div>--}}

                        {{--<!-- Description -->--}}
                        {{--<p>{{$top_news->short_description}}</p>                                        <!-- Read More -->--}}
                        {{--<a href="{{route('frontend.postdetail',$top_news->slug)}}" class="read-more">continue reading</a>--}}

                        {{--</div>--}}

                        {{--</div>--}}
                        {{--</div><!-- Post End -->--}}

                        {{--@endforeach--}}

                        {{--<div class="pages text-center">--}}
                        {{--{{$data['top_news']->links()}}--}}
                        {{--</div>--}}

                        {{--</div><!-- Post Block Body End -->--}}

                    </div><!-- Post Block Wrapper End -->

                </div>

                <!-- Sidebar Start -->
                <div class="col-lg-4 col-12 mb-50">
                    <div class="row">

                        <!-- Single Sidebar -->
                        <div class="single-sidebar col-lg-12 col-md-6 col-12">

                            <!-- Sidebar Block Wrapper -->
                            <div class="sidebar-block-wrapper">

                                <!-- Sidebar Block Head Start -->
                                <div class="head feature-head">

                                    <!-- Title -->
                                    <h4 class="title">Follow Us</h4>

                                </div><!-- Sidebar Block Head End -->

                                <!-- Sidebar Block Body Start -->
                                <div class="body">

                                    <div class="sidebar-social-follow">
                                        <div>
                                            <a href="#" class="facebook">
                                                <i class="fa fa-facebook"></i>
                                                <h3>40,583</h3>
                                                <span>Fans</span>
                                            </a>
                                        </div>
                                        <div>
                                            <a href="#" class="google-plus">
                                                <i class="fa fa-google-plus"></i>
                                                <h3>36,857</h3>
                                                <span>Followers</span>
                                            </a>
                                        </div>
                                        <div>
                                            <a href="#" class="twitter">
                                                <i class="fa fa-twitter"></i>
                                                <h3>50,883</h3>
                                                <span>Followers</span>
                                            </a>
                                        </div>
                                        <div>
                                            <a href="#" class="dribbble">
                                                <i class="fa fa-dribbble"></i>
                                                <h3>4,743</h3>
                                                <span>Followers</span>
                                            </a>
                                        </div>
                                    </div>

                                </div><!-- Sidebar Block Body End -->

                            </div>

                        </div>

                        <!-- Single Sidebar -->
                        <div class="single-sidebar col-lg-12 col-md-6 col-12">

                            <!-- Sidebar Banner -->
                            <a href="#" class="sidebar-banner"><img src="{{asset('images/advertisement/' . $data['advertisements'][0]->image)}}" alt="Sidebar Banner"></a>

                        </div>

                        <!-- Single Sidebar -->
                        <div class="single-sidebar col-lg-12 col-md-6 col-12">

                            <!-- Sidebar Banner -->
                            <a href="#" class="sidebar-banner"><img src="{{asset('images/advertisement/' . $data['advertisements'][1]->image)}}"" alt="Sidebar Banner"></a>

                        </div>

                        <!-- Single Sidebar -->
                        <div class="single-sidebar col-lg-12 col-md-6 col-12">

                            <div class="sidebar-subscribe">
                                <h4>Subscribe To <br>Our <span>Update</span> News</h4>
                                <p>Adipiscing elit. Fusce sed mauris arcu. Praesent ut augue imperdiet, semper lorem id.</p>
                                <!-- Newsletter Form -->
                                <form action="http://devitems.us11.list-manage.com/subscribe/post?u=6bbb9b6f5827bd842d9640c82&amp;id=05d85f18ef" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="subscribe-form validate" target="_blank" novalidate>
                                    <div id="mc_embed_signup_scroll">
                                        <label for="mce-EMAIL" class="d-none">Subscribe to our mailing list</label>
                                        <input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="Your email address" required>
                                        <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_6bbb9b6f5827bd842d9640c82_05d85f18ef" tabindex="-1" value=""></div>
                                        <button type="submit" name="subscribe" id="mc-embedded-subscribe" class="button">submit</button>
                                    </div>
                                </form>
                            </div>

                        </div>

                    </div>
                </div><!-- Sidebar End -->

            </div><!-- Feature Post Row End -->

        </div>
    </div><!-- Post Section End -->
@endsection