<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Configuration extends Model
{
    protected $table = 'configurations';

    protected $fillable = ['name','editor','sub_editor','email','phone','website',
        'address','fav_icon','fb_link','twitter_link','insta_link','youtube_link','google_link',
        'pinterest_link','vimeo_link','created_by', 'updated_by'];

}
