@extends('layouts.backend')
@section('title','Category Show page')

@section('content')
    <section class="content-header">
        <h1>
            Category Management
            <a href="{{route('category.create')}}" class="btn btn-success">
                <i class="fa fa-plus"></i>
                Create
            </a>

            <a href="{{route('category.index')}}" class="btn btn-info">
                <i class="fa fa-list"></i>
                List
            </a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('category.index')}}">Category</a></li>
            <li class="active">Show page</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Show Page</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                @include('includes.flash')
               <table class="table table-bordered">
                   <thead>
                   <tr>
                       <th>Name</th>
                       <td>{{$data['categories']->name}}</td>

                   </tr>

                   <tr>
                       <th>Rank</th>
                       <td>{{$data['categories']->rank}}</td>
                   </tr>
                   <tr>
                       <th>Status</th>
                       <td>
                           @if($data['categories']->status==1)
                               <span class="label label-success">Active</span>
                           @else
                               <span class="label label-danger">Deactive</span>
                               @endif
                       </td>
                   </tr>
                   <tr>
                       <th>Slug</th>
                       <td>{{$data['categories']->slug}}</td>
                   </tr>
                   <tr>
                       <th>feature_image</th>


                       <td>
                           <img src="{{asset("images/categories/".$data['categories']->feature_image)}}" alt=""></td>
                           </td>
                   </tr>
                   <tr>
                       <th>Meta Title</th>
                       <td>{{$data['categories']->meta_title}}</td>
                   </tr>
                   <tr>
                       <th>Meta Description</th>
                       <td>{{$data['categories']->meta_description}}</td>
                   </tr>
                   <tr>
                       <th>Description</th>
                       <td>{{$data['categories']->description}}</td>
                   </tr>
                   <tr>
                       <th>Meta keyword</th>
                       <td>{{$data['categories']->meta_keyword}}</td>
                   </tr>
                   <tr>
                       <th>Created By</th>
                       <td>{{App\User::find($data['categories']->created_by)->name}}</td>
                   </tr>
                   <tr>
                       <th>Created At</th>
                       <td>{{$data['categories']->meta_keyword}}</td>                   </tr>
                   <tr>
                       <th>Updated At</th>
                       <td></td>
                   </tr>
                   <tr>
                       <th>Updated By</th>
                       <td>
                           @if(!empty($data['categories']->updated_by))
                               {{\App\User::find($data['categories']->updated_by)->name}}
                           @endif
                       </td>
                   </tr>
                   <tr>
                       <th>Action</th>
                       @foreach($data['category'] as $category)
                           <td>
                               <a href="{{route('category.edit',$category->id)}}" class="btn btn-warning">
                                   <i class="fa fa-pencil"></i>
                                   Edit
                               </a>
                               <form action="{{route('category.destroy',$category->id)}}" method="post"
                                     onsubmit="return confirm('Are you sure?')">
                                   @csrf
                                   <input type="hidden" name="_method" value="DELETE"/>
                                   <button class="btn-danger"><i class="fa fa-trash"></i>Delete</button>
                               </form>
                           </td>
                       @endforeach
                   </tr>

                   </thead>
               </table>
            </div>

        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection