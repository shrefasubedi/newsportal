<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table ='categories';

    protected  $fillable = ['name','slug','rank','feature_image','status','meta_keyword','meta_description','description','meta_title','created_by','updated_by'];

    public function news(){
        return $this->hasMany(News::class);
    }
}
