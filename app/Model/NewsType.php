<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class NewsType extends Model
{
    protected $table ='newstypes';

    protected  $fillable = ['name','slug','feature_image','status','created_by','updated_by','deleted_at'];

    public function news(){
        return $this->belongsToMany(News::class,'news_newstype');
    }

}
