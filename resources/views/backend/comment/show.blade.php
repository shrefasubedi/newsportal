@extends('layouts.backend')
@section('title','Comment Show page')

@section('content')
    <section class="content-header">
        <h1>
            Comment Management
            <a href="{{route('comment.create')}}" class="btn btn-success">
                <i class="fa fa-plus"></i>
                Create
            </a>

            <a href="{{route('comment.index')}}" class="btn btn-info">
                <i class="fa fa-list"></i>
                List
            </a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('comment.index')}}">Comment</a></li>
            <li class="active">Show page</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Show page</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                @include('includes.flash')
               <table class="table table-bordered">
                   <thead>
                   <tr>
                       <th>News Id</th>
                       <td>{{$data['comments']->news_id}}</td>
                   </tr>
                   <tr>
                       <th>Name</th>
                       <td>{{$data['comments']->name}}</td>

                   </tr>
                   <tr>
                       <th>Email</th>
                       <td>{{$data['comments']->email}}</td>

                   </tr>
                   <tr>
                       <th>Comment</th>
                       <td>{{$data['comments']->comment}}</td>

                   </tr>


                   <tr>
                       <th>Status</th>
                       <td>
                           @if($data['comments']->status==1)
                               <span class="label label-success">Active</span>
                           @else
                               <span class="label label-danger">Deactive</span>
                               @endif
                       </td>
                   </tr>


                   <tr>
                       <th>Created At</th>
                       <td>{{$data['comments']->created_at}}</td>

                   </tr>
                   <tr>
                       <th>Updated At</th>
                       <td></td>
                   </tr>
                   <tr>
                       <th>Action</th>
                       @foreach($data['comment'] as $comment)
                           <td>
                               <a href="{{route('comment.edit',$comment->id)}}" class="btn btn-warning">
                                   <i class="fa fa-pencil"></i>
                                   Edit
                               </a>
                               <form action="{{route('comment.destroy',$comment->id)}}" method="post"
                                     onsubmit="return confirm('Are you sure?')">
                                   @csrf
                                   <input type="hidden" name="_method" value="DELETE"/>
                                   <button class="btn-danger"><i class="fa fa-trash"></i>Delete</button>
                               </form>
                           </td>
                       @endforeach
                   </tr>

                   </thead>
               </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                Footer
            </div>
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection