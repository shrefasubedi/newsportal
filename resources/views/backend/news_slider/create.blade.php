@extends('layouts.backend')
@section('title','NewsSlider Create page')
@section('js')
    <script>
        $("#title").keyup(function(){
            var Text = $(this).val();
            Text = Text.toLowerCase();
            Text = Text.replace(/[^a-zA-Z0-9]+/g,'-');
            $("#slug").val(Text);
        });

        $(document).ready(function () {

            $('.select2').select2();
        });
    </script>
    @endsection
@section('content')
    <section class="content-header">
        <h1>
            NewsSlider Management
            <a href="{{route('news_slider.index')}}" class="btn btn-success">
                <i class="fa fa-plus"></i>
                List
            </a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('news_slider.index')}}">NewsSlider</a></li>
            <li class="active">Create page</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <form action="{{route('news_slider.store')}}" method="post" enctype="multipart/form-data">
            <div class="box-header with-border">
                <h3 class="box-title">Create page</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
                <div class="box-body">
                    @include('includes.flash')
                    @include('includes.error')
                    {!! Form::open(['route' => 'news_slider.store', 'method' => 'post','files' => true]) !!}
                    @include('backend.news_slider.mainform')


                    <div class="form-group">

                        <button type="submit" class="btn btn-success"   value="Save NewsSlider"><i class="fa fa-save"></i>Save NewsSlider</button>
                        <button type="submit" class="btn btn-danger"   value="Clear"><i class="fa fa-recycle"></i>Cancel</button>
                    </div>
                    {!! Form::close() !!}

                </div>

            </form>
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection