@extends('layouts.backend')
@section('title','Advertisement page')

@section('content')
    <section class="content-header">
        <h1>
            Advertisement Management
            <a href="{{route('advertisement.create')}}" class="btn btn-success">
                <i class="fa fa-plus"></i>
                Create
            </a>

            <a href="{{route('advertisement.index')}}" class="btn btn-info">
                <i class="fa fa-list"></i>
                List
            </a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('advertisement.index')}}">Advertisement</a></li>
            <li class="active">Show page</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Show Page</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                @include('includes.flash')
               <table class="table table-bordered">
                   <thead>
                   <tr>
                       <th>Page Id</th>
                       <td>{{$data['advertisements']->page_id}}</td>
                   </tr>
                   <tr>
                       <th>Title</th>
                       <td>{{$data['advertisements']->title}}</td>

                   </tr>

                   <tr>
                       <th>Rank</th>
                       <td>{{$data['advertisements']->rank}}</td>
                   </tr>
                   <tr>
                       <th>Status</th>
                       <td>
                           @if($data['advertisements']->status==1)
                               <span class="label label-success">Active</span>
                           @else
                               <span class="label label-danger">Deactive</span>
                               @endif
                       </td>
                   </tr>
                   <tr>
                       <th>Image</th>


                       <td>
                           <img src="{{asset("images/advertisements/".$data['advertisements']->image)}}" alt=""></td>
                       </td>
                   </tr>
                   <tr>
                       <th>Description</th>
                       <td>{{$data['advertisements']->description}}</td>
                   </tr>
                   <tr>
                       <th>Expired At</th>
                       <td>{{$data['advertisements']->expired_at}}</td>
                   </tr>

                   <tr>
                       <th>Created At</th>
                       <td></td>
                   </tr>
                   <tr>
                       <th>Updated At</th>
                       <td></td>
                   </tr>

                   <tr>
                       <th>Action</th>
                       @foreach($data['advertisement'] as $advertisement)
                           <td>
                               <a href="{{route('advertisement.edit',$advertisement->id)}}" class="btn btn-warning">
                                   <i class="fa fa-pencil"></i>
                                   Edit
                               </a>
                               <form action="{{route('advertisement.destroy',$advertisement->id)}}" method="post"
                                     onsubmit="return confirm('Are you sure?')">
                                   @csrf
                                   <input type="hidden" name="_method" value="DELETE"/>
                                   <button class="btn-danger"><i class="fa fa-trash"></i>Delete</button>
                               </form>
                           </td>
                       @endforeach
                   </tr>
                   </thead>
               </table>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection