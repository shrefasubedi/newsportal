<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\CommentRequest;
use App\Model\Comment;
use App\Model\News;
use App\Model\NewsSlider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['news']=News::all();
        $data['comments'] = Comment::all();

        return view('backend.comment.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['news'] = News::pluck('title','id');
        return view('backend.comment.create',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommentRequest $request)
    {
        //add extra column
        $request->request->add(['created_by' => Auth::user()->id]);
        //save data
        $comment = Comment::create($request->all());
        if ($comment){
            $request->session()->flash('success_message','Comment Insert Success!!');
            return redirect()->route('comment.index');
        }else{
            $request->session()->flash('error_message','Comment Insert Failed!!');
            return redirect()->route('comment.create');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['news'] = News::pluck('title','id');
        $data['comments']  = Comment::find($id);
        $data['comment']=Comment::where('id',$id)->get();
        return view('backend.comment.show',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['news'] = News::pluck('title','id');
        $data['comments'] = Comment::find($id);
        return view('backend.comment.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CommentRequest $request, $id)
    {
        $comment = Comment::find($id);
        $request->request->add(['updated_by' => Auth::user()->id]);
        //save data
        $comment = $comment->update($request->all());
        if ($comment){
            $request->session()->flash('success_message','Comment Update Success!!');

        }else{
            $request->session()->flash('error_message','Comment Update Failed!!');

        }
        return redirect()->route('comment.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $comment = Comment::find($id);

        if ($comment->delete()){
            $request->session()->flash('success_message','News Delete Success!!');

        }else{
            $request->session()->flash('error_message','News Delete Failed!!');

        }
        return redirect()->route('comment.index');
    }

}
