<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comments';

    protected $fillable = ['news_id', 'name', 'email', 'website', 'message', 'status'];

    public function news()
    {
        return $this->belongsTo(News::class);
    }
}
