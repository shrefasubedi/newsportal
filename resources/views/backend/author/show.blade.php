@extends('layouts.backend')
@section('title','Author Create page')

@section('content')
    <section class="content-header">
        <h1>
            Author Management
            <a href="{{route('author.create')}}" class="btn btn-success">
                <i class="fa fa-plus"></i>
                Create
            </a>

            <a href="{{route('author.edit')}}" class="btn btn-info">
                <i class="fa fa-list"></i>
                Edit
            </a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Author</a></li>
            <li class="active">Create page</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Title</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                @include('includes.flash')
               <table class="table table-bordered">
                   <thead>
                   <tr>
                       <th>Name</th>
                       <td>{{$data['authors']->name}}</td>

                   </tr>

                   <tr>
                       <th>Rank</th>
                       <td>{{$data['authors']->delegation}}</td>
                   </tr>
                   <tr>
                       <th>Status</th>
                       <td>
                           @if($data['authors']->status==1)
                               <span class="label label-success">Active</span>
                           @else
                               <span class="label label-danger">Deactive</span>
                               @endif
                       </td>
                   </tr>
                   <tr>
                       <th>image</th>


                       <td>
                           <img src="{{asset("images/authors/".$data['authors']->image)}}" alt=""></td>
                       </td>
                   </tr>

                   <tr>
                       <th>Created At</th>
                       <td>{{$data['authors']->created_at}}</td>                   </tr>
                   <tr>
                       <th>Updated At</th>
                       <td></td>
                   </tr>


                   </thead>
               </table>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection