<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class NewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id'=>'required|integer',
            'title'=> 'required|string',
            'slug'=> 'required|string|max:191|unique:categories'.(request()->method()=="POST" ? '' : 'slug,'.$this->id),
            'feature_image'=>'max:1024',
            'meta_keyword'=> 'max:191',

        ];
    }
    function messages()
    {
        return[
            'category_id'=>'required}integer',
            'title.required'=> 'Please Enter title',
            'slug.required'=> 'Please Enter String value',
            'meta_keyword.max'=> 'Please Enter String',
        ];
    }
}
