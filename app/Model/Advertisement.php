<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Advertisement extends Model
{
    protected $table ='advertisements';

    protected  $fillable = ['page_id','title','rank','image','status','description','expired_at'];

}
