<div class="menu-section section bg-dark">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="menu-section-wrap">

                    <!-- Main Menu Start -->
                    <div class="main-menu float-left d-none d-md-block">
                        <nav>
                            <ul>

                                <li class="{{ url()->current() == url('/')?'active': ''  }}"><a href="{{route('frontend.index')}}">Home</a>

                                </li>
                                @foreach($data['categories'] as $category)
                                    <li class="{url()->current() == url('category' , $category->id)?'active': ''  }}}"><a href="{{route('frontend.categorydetail',$category->slug)}}">{{$category->name}}</a></li>
                                @endforeach


                            </ul>
                        </nav>
                    </div><!-- Main Menu Start -->

                    <div class="mobile-logo d-none d-block d-md-none"><a href="{{route('frontend.index')}}"><img src="img/logo-white.png" alt="Logo"></a></div>

                    <!-- Header Search -->
                    <div class="header-search float-right">

                        <!-- Search Toggle -->
                        <button class="header-search-toggle"><i class="fa fa-search"></i></button>

                        <!-- Header Search Form -->
                        <div class="header-search-form">
                            <form action="{{route('frontend.search' )}}" method="get">
                                <input type="text" name="search" placeholder="Search Here">
                            </form>
                        </div>

                    </div>

                    <!-- Mobile Menu Wrap -->
                    <div class="mobile-menu-wrap d-none">
                        <nav>
                            <ul>

                                <li class="active"><a href="{{route('frontend.index')}}">Home</a>
                                </li>
                                @foreach($data['categories'] as $category)
                                    <li><a href="category-lifestyle.html">{{$category->name}}</a></li>
                                @endforeach



                            </ul>
                        </nav>
                    </div>

                    <!-- Mobile Menu -->
                    <div class="mobile-menu"></div>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="breaking-news-section section">
    <div class="container">
        <div class="row">
            <div class="col-12">

                <!-- Breaking News Wrapper Start -->
                <div class="breaking-news-wrapper">

                    <!-- Breaking News Title -->
                    <h5 class="breaking-news-title float-left">Breaking News</h5>

                    <!-- Breaking Newsticker Start -->
                    <ul class="breaking-news-ticker float-left">
                        @for ($i = 0 ; $i < 3; $i++)
                            <li><a href="{{route('frontend.postdetail',$data['breaking_news'][$i]->slug)}}">{{$data['breaking_news'][$i]->meta_keyword}}</a></li>
                        @endfor
                    </ul><!-- Breaking Newsticker Start -->

                    <!-- Breaking News Nav -->
                    <div class="breaking-news-nav">
                        <button class="news-ticker-prev"><i class="fa fa-angle-left"></i></button>
                        <button class="news-ticker-next"><i class="fa fa-angle-right"></i></button>
                    </div>

                </div><!-- Breaking News Wrapper End -->

            </div>
        </div>
    </div>
</div><!-- Breaking News Section End -->
