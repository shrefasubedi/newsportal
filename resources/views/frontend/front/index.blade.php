@extends('layouts.frontend')
@section('content')
    <!-- Hero Section Start -->
    <div class="hero-section section mt-30 mb-30">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="row row-1">

                        <div class="order-lg-2 col-lg-6 col-12">

                            <!-- Hero Post Slider Start -->
                            <div class="post-carousel-1">
                            @foreach ($data['news_sliders'] as $news_slider)
                                <!-- Overlay Post Start -->
                                    <div class="post post-large post-overlay hero-post">
                                        <div class="post-wrap">

                                            <!-- Image -->


                                            <div class="image"><img src="{{asset('images/news_sliders/' . $news_slider->image)}}"  alt="post"></div>

                                            <!-- Category -->

                                            <a href="{{route('frontend.postdetail',$news_slider->slug)}}" class="category politic">{{$news_slider->title}}</a>

                                            <!-- Content -->
                                            <div class="content">

                                                <!-- Title -->
                                                <h2 class="title"><a href="{{route('frontend.postdetail',$news_slider->slug)}}">{{$news_slider->meta_keyword}}</a></h2>

                                                <!-- Meta -->
                                                <div class="meta fix">
                                                    <span class="meta-item date"><i class="fa fa-clock-o"></i>{{$news_slider->created_at}}</span>
                                                </div>
                                            </div>


                                        </div>
                                    </div><!-- Overlay Post End -->
                                @endforeach
                            </div><!-- Hero Post Slider End -->
                        </div>
                        <div class="order-lg-1 col-lg-3 col-12">
                            <div class="row row-1">
                            {{--@foreach ($data['latest_news'] as $latest_news)--}}

                            <!-- Overlay Post Start -->
                                @for ($i = 0 ; $i < 2; $i++)
                                    <div class="post post-overlay hero-post col-lg-12 col-md-6 col-12">

                                        <div class="post-wrap">

                                            <!-- Image -->
                                            <div class="image"><img src="{{asset('images/news/' . $data['latest_news'][$i]->feature_image)}}" alt="post"></div>
                                            <!-- Category -->
                                            <a href="{{route('frontend.postdetail',$data['latest_news'][$i]->slug)}}" class="category sports">{{$data['latest_news'][$i]->title}}</a>

                                            <!-- Content -->
                                            <div class="content">

                                                <!-- Title -->
                                                <h4 class="title"><a href="{{route('frontend.postdetail',$data['latest_news'][$i]->slug)}}">{{$data['latest_news'][$i]->meta_keyword}}</a></h4>

                                                <!-- Meta -->
                                                <div class="meta fix">
                                                    <span class="meta-item date"><i class="fa fa-clock-o"></i>{{$data['latest_news'][$i]->created_at}}</span>
                                                </div>

                                            </div>

                                        </div>
                                    </div><!-- Overlay Post End -->
                                @endfor
                            </div>

                        </div>


                        <div class="order-lg-3 col-lg-3 col-12">
                            <div class="row row-1">

                                <!-- Overlay Post Start -->
                                @for ($i = 2 ; $i < 4; $i++)
                                    <div class="post post-overlay gradient-overlay-1 hero-post col-lg-12 col-md-6 col-12">
                                        <div class="post-wrap">

                                            <!-- Image -->
                                            <div class="image"><img src="{{asset('images/news/' . $data['latest_news'][$i]->feature_image)}}" alt="post"></div>

                                            <!-- Category -->
                                            <a href="{{route('frontend.postdetail',$data['latest_news'][$i]->slug)}}" class="category sports">{{$data['latest_news'][$i]->title}}</a>

                                            <!-- Content -->
                                            <div class="content">

                                                <!-- Title -->
                                                <h4 class="title"><a href="{{route('frontend.postdetail',$data['latest_news'][$i]->slug)}}">{{$data['latest_news'][$i]->meta_keyword}}</a></h4>

                                                <!-- Meta -->
                                                <div class="meta fix">
                                                    <span class="meta-item date"><i class="fa fa-clock-o"></i>{{$data['latest_news'][$i]->created_at}}</span>
                                                </div>

                                            </div>

                                        </div>
                                    </div><!-- Overlay Post End -->
                                @endfor

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div><!-- Hero Section End -->

    <!-- Popular Section Start -->
    <div class="popular-section section pt-50 pb-50">
        <div class="container">
            <div class="row">
                <div class="col">

                    <!-- Popular Post Slider Start -->
                    <div class="popular-post-slider">
                    @foreach ($data['latest_news'] as $latest_news)
                        <!-- Post Start -->
                            <div class="post post-small post-list post-dark popular-post">
                                <div class="post-wrap">

                                    <!-- Image -->
                                    <a class="image" href="{{route('frontend.postdetail',$latest_news->slug)}}"><img src="{{asset('images/news/' . $latest_news->feature_image)}}" alt="post"></a>

                                    <!-- Content -->
                                    <div class="content fix">

                                        <!-- Title -->
                                        <h5 class="title"><a href="{{route('frontend.postdetail',$latest_news->slug)}}">{{$latest_news->meta_keyword}}</a></h5>

                                        <!-- Description -->
                                        <p>{{$latest_news->meta_description}}</p>

                                        <!-- Read More -->
                                        <a href="{{route('frontend.postdetail',$latest_news->slug)}}" class="read-more">continue reading</a>

                                    </div>

                                </div>
                            </div><!-- Post Start -->
                        @endforeach



                    </div><!-- Popular Post Slider End -->

                </div>
            </div>
        </div>
    </div><!-- Popular Section End -->

    <!-- Post Section Start -->
    <div class="post-section section mt-50">
        <div class="container">

            <!-- Feature Post Row Start -->
            <div class="row">

                <div class="col-lg-8 col-12 mb-50">

                    <!-- Post Block Wrapper Start -->
                    <div class="post-block-wrapper">

                        <!-- Post Block Head Start -->
                        <div class="head feature-head">

                            <!--Title -->
                            <h4 class="title">Featured News</h4>

                            <!-- Tab List Start -->
                            <ul class="post-block-tab-list feature-post-tab-list nav d-none d-md-block">

                                @foreach($data['categories'] as $index => $category)
                                    <li><a @if($index == 0) class="active" @endif data-toggle="tab" href="{{route('frontend.postdetail',$category->slug)}}#feature-cat-{{$index}}" class="" aria-expanded="false">{{$category->name}}</a></li>
                                @endforeach
                            </ul><!-- Tab List End -->

                        </div><!-- Post Block Head End -->

                        <!-- Post Block Body Start -->
                        <div class="body pb-0">

                            <!-- Tab Content Start-->
                            <div class="tab-content">
                            @foreach($data['categories'] as $index => $category)

                                @php ($categoryNews = $category->news()->where('status',1)->limit(4)->get()->toArray())


                                <!-- Tab Pane Start-->


                                    <div class="tab-pane fade @if($index == 0) active show @endif" id="feature-cat-{{$index}}" aria-expanded="true">

                                        <div class="row">

                                        @if(count($categoryNews) > 0 )
                                            <!-- Post Wrapper Start -->
                                                <div class="col-md-6 col-12 mb-20">

                                                    <!-- Post Start -->
                                                    <div class="post feature-post post-separator-border">
                                                        <div class="post-wrap">

                                                            <!-- Image -->
                                                            <a class="image" href="{{route('frontend.postdetail',$categoryNews[0]['slug'])}}"><img src="{{asset('images/news/' . $categoryNews[0]['feature_image'])}}" alt="post"></a>

                                                            <!-- Content -->
                                                            <div class="content">

                                                                <!-- Title -->
                                                                <h4 class="title"><a href="{{route('frontend.postdetail',$categoryNews[0]['slug'])}}">{{$categoryNews[0]['title']}}</a></h4>

                                                                <!-- Meta -->
                                                                <div class="meta fix">
                                                                    <a href="#" class="meta-item author"><i class="fa fa-user"></i>Sathi Bhuiyan</a>
                                                                    <span class="meta-item date"><i class="fa fa-clock-o"></i>{{$categoryNews[0]['created_at']}}</span>
                                                                    <a href="#" class="meta-item comment"><i class="fa fa-comments"></i>(34)</a>
                                                                </div>

                                                                <!-- Description -->
                                                                <p>{{$categoryNews[0]['short_description']}}</p>

                                                            </div>

                                                        </div>
                                                    </div><!-- Post End -->



                                                </div><!-- Post Wrapper End -->

                                            @php(array_shift($categoryNews))
                                            <!-- Small Post Wrapper Start -->
                                                <div class="col-md-6 col-12 mb-20">

                                                @foreach($categoryNews as $news)
                                                    <!-- Post Small Start -->
                                                        <div class="post post-small post-list feature-post post-separator-border">
                                                            <div class="post-wrap">

                                                                <!-- Image -->
                                                                <a class="image" href="{{route('frontend.postdetail',$categoryNews[0]['slug'])}}"><img src="{{asset('images/news/' . $news['feature_image'])}}" alt="post"></a>

                                                                <!-- Content -->
                                                                <div class="content">

                                                                    <!-- Title -->
                                                                    <h5 class="title"><a href="{{route('frontend.postdetail',$categoryNews[0]['slug'])}}">{{$news['title']}}</a></h5>

                                                                    <!-- Meta -->
                                                                    <div class="meta fix">
                                                                        <span class="meta-item date"><i class="fa fa-clock-o"></i>{{$news['created_at']}}</span>
                                                                    </div>

                                                                </div>

                                                            </div>
                                                        </div><!-- Post Small End -->
                                                @endforeach
                                                <!-- Post Small Start -->



                                                </div><!-- Small Post Wrapper End -->
                                            @else
                                                <div class="col-md-12">
                                                    <p class="alert alert-danger">News not found</p>
                                                </div>
                                            @endif

                                        </div>

                                    </div><!-- Tab Pane End-->


                                @endforeach

                            </div><!-- Tab Content End-->

                        </div><!-- Post Block Body End -->

                    </div><!-- Post Block Wrapper End -->

                </div>

                <!-- Sidebar Start -->
                <div class="col-lg-4 col-12 mb-50">
                    <div class="row">



                        <!-- Single Sidebar -->
                        <div class="single-sidebar col-lg-12 col-md-6 col-12">

                            <!-- Sidebar Banner -->
                            <a href="#" class="sidebar-banner"><img  src="{{asset('images/advertisements/' . $data['advertisements'][1]->image)}}" alt="Sidebar Banner"></a>

                        </div>

                    </div>
                </div><!-- Sidebar End -->

            </div><!-- Feature Post Row End -->


            @foreach($data['categories'] as $category)
                @php ($categoryNews = $category->news()->where('status',1)->limit(4)->get()->toArray())

                <div class="row ">

                    <div class="col-lg-12 col-12 mb-50">

                        <!-- Post Block Wrapper Start -->
                        <div class="post-block-wrapper">

                            <!-- Post Block Head Start -->
                            <div class="head life-style-head">

                                <!-- Title -->
                                <h4 class="title">{{$category->name}}</h4>
                                &nbsp;
                                <a href="{{route('frontend.categorydetail',$category->slug)}}" class="read-more col-md-8">read more</a>





                            </div><!-- Post Block Head End -->

                            <!-- Post Block Body Start -->
                            <div class="body pb-0">

                                <!-- Tab Content Start-->
                                <div class="tab-content">

                                    <!-- Tab Pane Start-->
                                    <div class="tab-pane fade show active" >

                                        <div class="row">
                                        @if(count($categoryNews) > 0 )
                                            <!-- Post Wrapper Start -->
                                                <div class="col-md-6 col-12 mb-20">

                                                    <!-- Overlay Post Start -->
                                                    <div class="post post-overlay life-style-post post-separator-border">
                                                        <div class="post-wrap">

                                                            <!-- Image -->
                                                            <div class="image"><img src="{{asset('images/news/' . $categoryNews[0]['feature_image'])}}" alt="post"></div>

                                                            <!-- Content -->
                                                            <div class="content">

                                                                <!-- Title -->
                                                                <h4 class="title"><a href="{{route('frontend.postdetail',$categoryNews[0]['slug'])}}">{{$categoryNews[0]['title']}}</a></h4>

                                                                <!-- Meta -->
                                                                <div class="meta fix">
                                                                    <a href="#" class="meta-item author"><i class="fa fa-user"></i> Sathi Bhuiyan</a>
                                                                    <span class="meta-item date"><i class="fa fa-clock-o"></i>10 March 2017</span>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div><!-- Overlay Post End -->


                                                </div><!-- Post Wrapper End -->

                                            @php(array_shift($categoryNews))
                                            <!-- Small Post Wrapper Start -->
                                                <div class="col-md-6 col-12 mb-20">
                                                @foreach($categoryNews as $news)
                                                    <!-- Small Post Start -->
                                                        <div class="post post-small post-list life-style-post post-separator-border">
                                                            <div class="post-wrap">

                                                                <!-- Image -->
                                                                <a class="image" href="{{route('frontend.postdetail',$categoryNews[0]['slug'])}}"><img src="{{asset('images/news/' . $news['feature_image'])}}" alt="post"></a>

                                                                <!-- Content -->
                                                                <div class="content">

                                                                    <!-- Title -->
                                                                    <h5 class="title"><a href="{{route('frontend.postdetail',$categoryNews[0]['slug'])}}">{{$news['title']}}</a></h5>

                                                                    <!-- Meta -->
                                                                    <div class="meta fix">
                                                                        <span class="meta-item date"><i class="fa fa-clock-o"></i>{{$news['created_at']}}</span>
                                                                    </div>

                                                                </div>

                                                            </div>
                                                        </div><!-- Small Post End -->
                                                    @endforeach
                                                </div><!-- Small Post Wrapper End -->
                                            @else
                                                No news for this category
                                            @endif
                                        </div>

                                    </div><!-- Tab Pane End-->


                                </div><!-- Tab Content End-->

                            </div><!-- Post Block Body End -->

                        </div><!-- Post Block Wrapper End -->

                    </div>
                </div><!-- Life Style Post Row End -->

        @endforeach






        <!-- Banner Row Start -->
            <div class="row mb-50">

                <div class="col-12">

                    <a href="#" class="post-middle-banner"><img src="{{asset('images/advertisements/' . $data['advertisements'][2]->image)}}" alt="Banner"></a>

                </div>

            </div><!-- Banner Row End -->

            <!-- Youtube Video Row Start -->
            <div class="row">

                <!-- Video Play List Start-->
                <div class="col-lg-8 col-12 mb-50">
                   {{--@foreach($data['videos'] as $video)--}}
                    <div class="youtube-video-playlist">

                        {{--<!-- Selector by Id -->--}}
                        {{--<div id="unix" data-ycp_title="Khobor HTML Template" data-ycp_channel="PL6XRrncXkMaWYv31tSdLGUKHDMQ2QtHTx"></div>--}}
                        {{--<!-- By ChannelId -->--}}
                        <iframe width="560" height="315" src="{{$data['videos'][0]->url}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        {{--{{$video->url}}--}}
                    </div>
               {{--@endforeach--}}
                </div>
                <!-- Video Play List End-->

                <!-- Sidebar Start -->
                <div class="col-lg-4 col-12 mb-50">
                    <div class="row">

                        <!-- Single Sidebar -->
                        <div class="single-sidebar col-lg-12 col-md-6 col-12">

                            <!-- Sidebar Block Wrapper -->
                            <div class="sidebar-block-wrapper">

                                <!-- Sidebar Block Head Start -->
                                <div class="head video-head">

                                    <!-- Title -->
                                    <h4 class="title">Hot Categories</h4>

                                </div><!-- Sidebar Block Head End -->

                                <!-- Sidebar Block Body Start -->
                                <div class="body">
                                    @foreach($data['categories'] as $category)
                                        <ul class="sidebar-category video-category">
                                            <li><a href="{{route('frontend.categorydetail',$category->slug)}}">{{$category->name}} ({{$category->news()->count()}})</a></li>
                                        </ul>
                                    @endforeach
                                </div><!-- Sidebar Block Body End -->

                            </div>

                        </div>

                        <!-- Single Sidebar -->
                        <div class="single-sidebar col-lg-12 col-md-6 col-12">

                            <div class="sidebar-subscribe">
                                <h4>Subscribe To <br>Our <span>Update</span> News</h4>
                                <p>Adipiscing elit. Fusce sed mauris arcu. Praesent ut augue imperdiet, semper lorem id.</p>
                                <!-- Newsletter Form -->
                                <form action="http://devitems.us11.list-manage.com/subscribe/post?u=6bbb9b6f5827bd842d9640c82&amp;id=05d85f18ef" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="subscribe-form validate" target="_blank" novalidate>
                                    <div id="mc_embed_signup_scroll">
                                        <label for="mce-EMAIL" class="d-none">Subscribe to our mailing list</label>
                                        <input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="Your email address" required>
                                        <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_6bbb9b6f5827bd842d9640c82_05d85f18ef" tabindex="-1" value=""></div>
                                        <button type="submit" name="subscribe" id="mc-embedded-subscribe" class="button">submit</button>
                                    </div>
                                </form>
                            </div>

                        </div>

                    </div>
                </div><!-- Sidebar End -->

            </div><!-- Youtube Video Banner Row End -->

            <!-- Technology, Fashion & Other Post Row Start -->
            <div class="row">

                <div class="col-lg-4 col-md-6 col-12 mb-50">

                    <!-- Post Block Wrapper Start -->
                    <div class="post-block-wrapper">

                        <!-- Post Block Head Start -->
                        <div class="head gadgets-head">

                            <!-- Title -->
                            <h4 class="title">Technology</h4>

                        </div><!-- Post Block Head End -->

                        <!-- Post Block Body Start -->
                        <div class="body">

                            <!-- Sidebar Post Slider Start -->
                            <div class="sidebar-post-carousel post-block-carousel gadgets-post-carousel">

                                <!-- Post Start -->
                                <div class="post gadgets-post">
                                    <div class="post-wrap">

                                        <!-- Image -->
                                        <a class="image" href="post-details.html"><img src="{{asset('frontend/img/post/post-43.jpg')}}" alt="post"></a>

                                        <!-- Content -->
                                        <div class="content">

                                            <!-- Title -->
                                            <h4 class="title"><a href="post-details.html">Sony Reveals The Xperia Z4, Its Latest Flagship Smartphone.</a></h4>

                                        </div>

                                    </div>
                                </div><!-- Post End -->

                                <!-- Post Start -->
                                <div class="post gadgets-post">
                                    <div class="post-wrap">

                                        <!-- Image -->
                                        <a class="image" href="post-details.html"><img src="{{asset('frontend/img/post/post-43.jpg')}}" alt="post"></a>

                                        <!-- Content -->
                                        <div class="content">

                                            <!-- Title -->
                                            <h4 class="title"><a href="post-details.html">Sony Reveals The Xperia Z4, Its Latest Flagship Smartphone.</a></h4>

                                        </div>

                                    </div>
                                </div><!-- Post End -->

                            </div><!-- Sidebar Post Slider End -->

                        </div><!-- Post Block Body End -->

                    </div><!-- Post Block Wrapper End -->

                </div>

                <div class="col-lg-4 col-md-6 col-12 mb-50">

                    <!-- Post Block Wrapper Start -->
                    <div class="post-block-wrapper">

                        <!-- Post Block Head Start -->
                        <div class="head fashion-head">

                            <!-- Title -->
                            <h4 class="title">fashion</h4>

                        </div><!-- Post Block Head End -->

                        <!-- Post Block Body Start -->
                        <div class="body">

                            <!-- Sidebar Post Slider Start -->
                            <div class="sidebar-post-carousel post-block-carousel fashion-post-carousel">

                                <!-- Post Start -->
                                <div class="post fashion-post">
                                    <div class="post-wrap">

                                        <!-- Image -->
                                        <a class="image" href="post-details.html"><img src="{{asset('frontend/img/post/post-44.jpg')}}" alt="post"></a>

                                        <!-- Content -->
                                        <div class="content">

                                            <!-- Title -->
                                            <h4 class="title"><a href="post-details.html">The scientific method of finding love on the beauty.</a></h4>

                                        </div>

                                    </div>
                                </div><!-- Post End -->

                                <!-- Post Start -->
                                <div class="post fashion-post">
                                    <div class="post-wrap">

                                        <!-- Image -->
                                        <a class="image" href="post-details.html"><img src="{{asset('frontend/img/post/post-44.jpg')}}" alt="post"></a>

                                        <!-- Content -->
                                        <div class="content">

                                            <!-- Title -->
                                            <h4 class="title"><a href="post-details.html">The scientific method of finding love on the beauty.</a></h4>

                                        </div>

                                    </div>
                                </div><!-- Post End -->

                            </div><!-- Sidebar Post Slider End -->

                        </div><!-- Post Block Body End -->

                    </div><!-- Post Block Wrapper End -->

                </div>

                <div class="col-lg-4 col-md-6 col-12 mb-50">

                    <!-- Post Block Wrapper Start -->
                    <div class="post-block-wrapper">

                        <!-- Post Block Head Start -->
                        <div class="head">

                            <!-- Title -->
                            <h4 class="title">Other News</h4>

                        </div><!-- Post Block Head End -->

                        <!-- Post Block Body Start -->
                        <div class="body">

                            <!-- Sidebar Post Slider Start -->
                            <div class="sidebar-post-carousel post-block-carousel">

                                <!-- Post Start -->
                                <div class="post">
                                    <div class="post-wrap">

                                        <!-- Image -->
                                        <a class="image" href="post-details.html"><img src="{{asset('frontend/img/post/post-45.jpg')}}" alt="post"></a>

                                        <!-- Content -->
                                        <div class="content">

                                            <!-- Title -->
                                            <h4 class="title"><a href="post-details.html">Tell me how to Achive your goal by creating a design.</a></h4>

                                        </div>

                                    </div>
                                </div><!-- Post End -->

                                <!-- Post Start -->
                                <div class="post">
                                    <div class="post-wrap">

                                        <!-- Image -->
                                        <a class="image" href="post-details.html"><img src="{{asset('frontend/img/post/post-45.jpg')}}" alt="post"></a>

                                        <!-- Content -->

                                        <div class="content">

                                            <!-- Title -->
                                            <h4 class="title"><a href="post-details.html">Tell me how to Achive your goal by creating a design.</a></h4>

                                        </div>

                                    </div>
                                </div><!-- Post End -->

                            </div><!-- Sidebar Post Slider End -->

                        </div><!-- Post Block Body End -->

                    </div><!-- Post Block Wrapper End -->

                </div>

            </div><!-- Technology, Fashion & Other Post Row End -->

        </div>
    </div><!-- Post Section End -->



@endsection