<?php

namespace App\Http\Controllers\Backend;

use App\Model\Author;
use App\Model\Category;
use App\Model\News;
use App\Model\NewsType;
use App\Model\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['news'] = News::all();
        return view('backend.news.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $data['categories'] = Category::pluck('name','id');
        $data['tags'] = Tag::pluck('name','id');
        $data['authors'] = Author::pluck('name','id');
        $data['newstype'] = NewsType::pluck('name','id');
        return view('backend.news.create',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!empty($request->file('photo'))) {

            $news_image = $request->file('photo');

            $image_name = uniqid() . '.' . $news_image->getClientOriginalExtension();
            $destinationPath = public_path('/images/news');
            $news_image->move($destinationPath, $image_name);
            $request->request->add(['feature_image' => $image_name]);
        }

        //add extra column
        $request->request->add(['created_by' => Auth::user()->id]);
        //save data
        $news = News::create($request->all());
        if ($news){
            $news->tags()->attach($request->input('tag_id'));
            $news->newstypes()->attach($request->input('newstype_id'));
            $request->session()->flash('success_message','News Insert Success!!');
            return redirect()->route('news.index');
        }else{
            $request->session()->flash('error_message','News Insert Failed!!');
            return redirect()->route('news.create');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['news']  = News::find($id);
        $data['new']  = News::where('id',$id)->get();
        return view('backend.news.show',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['categories'] = Category::pluck('name','id');
        $data['tags'] = Tag::pluck('name','id');
        $data['authors'] = Author::pluck('name','id');
        $data['newstype'] = NewsType::pluck('name','id');
        $data['news']=News::find($id);

        return view('backend.news.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!empty($request->file('photo'))) {

            $news_image = $request->file('photo');

            $image_name = uniqid() . '.' . $news_image->getClientOriginalExtension();
            $destinationPath = public_path('/images/news');
            $news_image->move($destinationPath, $image_name);
            $request->request->add(['feature_image' => $image_name]);
        }


        $news = News::find($id);
        $request->request->add(['updated_by' => Auth::user()->id]);
        //save data
        $news = $news->update($request->all());
        if ($news){
            $request->session()->flash('success_message','News Update Success!!');

        }else{
            $request->session()->flash('error_message','News Update Failed!!');

        }
        return redirect()->route('news.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request ,$id)
    {
        $news = News::find($id);

        if ($news->delete()){
            $request->session()->flash('success_message','News Delete Success!!');

        }else{
            $request->session()->flash('error_message','News Delete Failed!!');

        }
        return redirect()->route('news.index');
    }
    public function search(Request $request)
    {
        $search = $request->get('search');
//        $events = DB::table('events')->where('name', 'like', '%'.$search.'%');
//        $data['events']=Event::all();
        $data['news'] = News::where('title', 'LIKE', '%'.$search.'%')->get();
        return view('backend.news.search',compact('data'));
    }
}
