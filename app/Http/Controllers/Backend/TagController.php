<?php

namespace App\Http\Controllers\Backend;

use App\Model\News;
use App\Model\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['tags'] = Tag::all();
        return view('backend.tag.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.tag.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//dd($request->all());
        //add login user id to request object
        // $request->request->add(['created_by' =>Auth::user()->id]);
        $request->request->add(['created_by' =>Auth::user()->id]);
        //dd($request->all());

        $tag = Tag::create($request->all());
        if ($tag){
            $request->session()->flash('success_message','Tag Created Successfully');
            return redirect()->route('tag.index');
        }else{
            $request->session()->flash('error_message','Tag Creation failed');
            return redirect()->route('tag.create');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //single data sho only 'tag ' written
        $data['tag'] = Tag::find($id);
        $data['tags']=Tag::where('id',$id)->get();
        return view('backend.tag.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
//single data sho only 'tag ' written
        //cpact for passing data
        $data['news'] = News::find($id);
        return view('backend.news.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());
        //add login user id to request object
        // $request->request->add(['created_by' =>Auth::user()->id]);
        $request->request->add(['updated_by' =>Auth::user()->id]);
        //dd($request->all());
        //select data from tag table using tag model

        $tag = Tag::find($id);
        if ($tag->update($request->all())){
            $request->session()->flash('success_message','Tag update Successfully');

        }else{
            $request->session()->flash('error_message','Tag Update failed');

        }
        return redirect()->route('tag.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $tag = Tag::find($id);
        if ($tag->delete()){
            $request->session()->flash('success_message','Tag deleted Successfully');

        }else{
            $request->session()->flash('error_message','Tag deleted failed');

        }
        return redirect()->route('tag.index');
    }
}
