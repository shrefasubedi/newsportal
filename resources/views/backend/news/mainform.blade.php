<div class="form-group">
    <label for="category_id">Category id</label>
    {!! Form::select('category_id', $data['categories'],null,['class' => 'form-control']); !!}

</div>
<div class="form-group">
    <label for="author_id">Author id</label>
    {!! Form::select('author_id', $data['authors'],null,['class' => 'form-control']); !!}

</div>
<div class="form-group">
    {!!  Form::label('title', 'Title'); !!}
    {!! Form::text('title', null,['class' => 'form-control','id' => 'title','placeholder' => 'enter title']); !!}
    @include('includes.single_field_validation',['field'=>'title'])
</div>

<div class="form-group">
    {!!  Form::label('slug', 'Slug'); !!}
    {!! Form::text('slug', null,['class' => 'form-control','id' => 'slug']); !!}

    @include('includes.single_field_validation',['field'=>'slug'])
</div>
<div class="form-group">
    {!!  Form::label('photo', 'Feature Image'); !!}
    {!! Form::file('photo'); !!}

    @include('includes.single_field_validation',['field'=>'photo'])
</div>

    <div class="form-group">
        <label for="select_tags">Select Tags </label>
        {!! Form::select('tag_id[]', $data['tags'] , isset($data['news']->tags)?$data['news']->tags:false,['multiple' => true,'class' =>'form-control select2']) !!}
    </div>
<div class="form-group">
    <label for="select_newstype">Select NewsType </label>
    {!! Form::select('newstype_id[]', $data['newstype'] , isset($data['news']->newstype)?$data['news']->newstype:false,['multiple' => true,'class' =>'form-control select2']) !!}
</div>



<div class="form-group ">
    {!!  Form::label('meta_keyword', 'Meta Keyword'); !!}
    {!! Form::textarea('meta_keyword', null,['class' => 'form-control','id' => '']); !!}

    @include('includes.single_field_validation',['field'=>'meta_keyword'])
</div>
<div class="form-group">
    {!!  Form::label('meta_description', 'Meta Description'); !!}
    {!! Form::textarea('meta_description', null,['class' => 'form-control','id' => '']); !!}

    @include('includes.single_field_validation',['field'=>'meta_description'])
</div>
<div class="form-group">
    {!!  Form::label('short_description', 'Short Description'); !!}
    {!! Form::textarea('short_description', null,['class' => 'form-control','id' => '']); !!}

    @include('includes.single_field_validation',['field'=>'short_description'])
</div>
<div class="form-group">
    {!!  Form::label('description', ' Description'); !!}
    {!! Form::textarea('description', null,['class' => 'form-control','id' => 'editor1']); !!}

    @include('includes.single_field_validation',['field'=>'description'])
</div>

<div class="form-group">
    {!!  Form::label('status', 'Status'); !!}
    {!! Form::radio('status', '1') !!} Active
    {!! Form::radio('status', '0',true) !!} De Active
</div>