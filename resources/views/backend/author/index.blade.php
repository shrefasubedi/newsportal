@extends('layouts.backend')
@section('title','Authors View page')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Authors Management
            <a href="{{route('author.create')}}" class="btn btn-success">
                <i class="fa fa-plus"></i>
                Create
            </a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Authors</a></li>
            <li class="active">View page</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">


                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                @include('includes.flash')
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>SN</th>
                        <th>Name</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php($i=1)
                    @foreach($data['authors'] as $author)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$author->name}}</td>
                            <td>
                                @if($author->status == 1)
                                    <span class="label label-success">Active</span>
                                @else
                                    <span class="label label-danger">De Active</span>
                                @endif
                            </td>

                            <td>
                                <a href="{{route('author.show',$author->id)}}"
                                   class="btn btn-info">
                                    <i class="fa fa-eye"></i>
                                    View
                                </a>
                                <a href="{{route('author.edit',$author->id)}}"
                                   class="btn btn-warning">
                                    <i class="fa fa-pencil"></i>
                                    Edit
                                </a>
                                <form action="{{route('author.destroy',$author->id)}}" method="post"
                                      onsubmit="return confirm('are you sure?')">
                                    @csrf
                                    <input type="hidden" name="_method" value="DELETE"/>
                                    <button class="btn btn-danger"><i class="fa fa-trash"></i>Delete </button>
                                </form>
                                Edit/Delete/View</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->

            <!-- /.box-footer-->
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection