<?php

namespace App\Http\Controllers\Backend;

use App\Model\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['pages'] = Page::all();
        return view('backend.page.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.page.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //add extra column
        $request->request->add(['created_by' => Auth::user()->id]);
        //save data
        $page = Page::create($request->all());
        if ($page){
            $request->session()->flash('success_message','Page Insert Success!!');
            return redirect()->route('page.index');
        }else{
            $request->session()->flash('error_message','Page Insert Failed!!');
            return redirect()->route('page.create');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['pages']  = Page::find($id);
        $data['page']=Page::where('id',$id)->get();
        return view('backend.page.show',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['pages'] = Page::find($id);
        return view('backend.page.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $page = Page::find($id);
        $request->request->add(['updated_by' => Auth::user()->id]);
        //save data
        $page = $page->update($request->all());
        if ($page){
            $request->session()->flash('success_message','Page Update Success!!');

        }else{
            $request->session()->flash('error_message','Page Update Failed!!');

        }
        return redirect()->route('page.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $page = Page::find($id);

        if ($page->delete()){
            $request->session()->flash('success_message','Page Delete Success!!');

        }else{
            $request->session()->flash('error_message','Page Delete Failed!!');

        }
        return redirect()->route('page.index');
    }
}
