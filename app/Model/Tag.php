<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $table ='tags';

    protected  $fillable = ['name','slug','rank','status','meta_keyword','meta_description','created_by','updated_by'];

    public function news(){
        return $this->belongsToMany(News::class);
    }
}


