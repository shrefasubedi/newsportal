@extends('layouts.backend')
@section('title','Advertisement Index page')

@section('content')
    <section class="content-header">
        <h1>
            Advertisements Management
            <a href="{{route('advertisement.create')}}" class="btn btn-info">
                <i class="fa fa-list"></i>
                List
            </a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('advertisement.create')}}">Advertisement</a></li>
            <li class="active">Index page</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Index Page</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
               @include('includes.flash')
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>SN</th>
                        <th>Page_id</th>
                        <th>Title</th>
                        <th>Rank</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    <tbody>
                    @php($i=1)

                    @foreach($data['advertisements'] as $advertisement)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$advertisement->page_id}}</td>
                            <td>{{$advertisement->title}}</td>
                            <td>{{$advertisement->rank}}</td>
                            <td>
                                @if($advertisement->status==1)
                                    <span class="label label-success">Active</span>
                                @else
                                    <span class="label label-danger">Deactive</span>
                                @endif
                            </td>
                            <td>
                                <a href="{{route('advertisement.show',$advertisement->id)}}" class="btn btn-info">
                                    <i class="fa fa-eye"></i>
                                    view
                                </a>

                                <a href="{{route('advertisement.edit',$advertisement->id)}}" class="btn btn-warning">
                                    <i class="fa fa-pencil"></i>
                                   Edit
                                </a>
                                <form action="{{route('advertisement.destroy',$advertisement->id)}}" method="post" onsubmit="return confirm('are you sure?')">
                                    @csrf
                                    <input type="hidden" name="_method" value="DELETE"/>
                                    <button class="btn btn-danger"><i class="fa fa-trash"></i>Delete</button>
                                </form>

                                Edit/Delete/View</td>
                        </tr>
                    @endforeach
                    </tbody>

                    </thead>
                </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                Footer
            </div>
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection