@extends('layouts.backend')
@section('title','Page  Index page')

@section('content')
    <section class="content-header">
        <h1>
            Pages Management
            <a href="{{route('page.create')}}" class="btn btn-info">
                <i class="fa fa-list"></i>
                Create
            </a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('page.create')}}">Page</a></li>
            <li class="active">Index page</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Index page</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                @include('includes.flash')
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>SN</th>
                        <th>Page_Name</th>
                        <th>Slug</th>
                        <th>URL</th>
                        <th>Static_Key</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    <tbody>
                    @php($i=1)

                    @foreach($data['pages'] as $page)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$page->page_name}}</td>
                            <td>{{$page->slug}}</td>
                            <td>{{$page->url}}</td>
                            <td>
                                @if($page->static_key==1)
                                    <span class="label label-success">Active</span>
                                @else
                                    <span class="label label-danger">Deactive</span>
                                @endif
                            </td>
                            <td>
                                @if($page->status==1)
                                    <span class="label label-success">Active</span>
                                @else
                                    <span class="label label-danger">Deactive</span>
                                @endif
                            </td>
                            <td>
                                <a href="{{route('page.show',$page->id)}}" class="btn btn-info">
                                    <i class="fa fa-eye"></i>
                                    view
                                </a>
                                <a href="{{route('page.edit',$page->id)}}" class="btn btn-warning">
                                    <i class="fa fa-pencil"></i>
                                    Edit
                                </a>



                                <form action="{{route('page.destroy',$page->id)}}" method="post" onsubmit="return confirm('are you sure?')">
                                    @csrf
                                    <input type="hidden" name="_method" value="DELETE"/>
                                    <button class="btn btn-danger"><i class="fa fa-trash"></i>Delete</button>
                                </form>
                                Edit/Delete/View</td>
                        </tr>
                    @endforeach
                    </tbody>

                    </thead>
                </table>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection