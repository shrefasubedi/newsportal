@extends('layouts.backend')
@section('title','News Slider Index page')

@section('content')
    <section class="content-header">
        <h1>
            NewsSlider Management
            <a href="{{route('news_slider.create')}}" class="btn btn-success">
                <i class="fa fa-plus"></i>
                Create
            </a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('news_slider.create')}}">NewsSlider</a></li>
            <li class="active">Index page</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Index page</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                @include('includes.flash')
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>SN</th>
                        <th>Title</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    <tbody>
                    @php($i=1)

                    @foreach($data['news_sliders'] as $news_slider)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$news_slider->title}}</td>

                            <td>
                                @if($news_slider->status==1)
                                    <span class="label label-success">Active</span>
                                @else
                                    <span class="label label-danger">Deactive</span>
                                @endif
                            </td>
                            <td>
                                <a href="{{route('news_slider.show',$news_slider->id)}}" class="btn btn-info">
                                    <i class="fa fa-eye"></i>
                                    view
                                </a>
                                <a href="{{route('news_slider.edit',$news_slider->id)}}" class="btn btn-warning">
                                    <i class="fa fa-pencil"></i>
                                    Edit
                                </a>



                                <form action="{{route('news_slider.destroy',$news_slider->id)}}" method="post" onsubmit="return confirm('are you sure?')">
                                    @csrf
                                    <input type="hidden" name="_method" value="DELETE"/>
                                    <button class="btn btn-danger"><i class="fa fa-trash"></i>Delete</button>
                                </form>
                                Edit/Delete/View</td>
                        </tr>
                    @endforeach
                    </tbody>

                    </thead>
                </table>
            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection