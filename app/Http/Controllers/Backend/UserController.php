<?php

namespace App\Http\Controllers\Backend;

use App\Model\Role;
use App\Model\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['users']= \App\Model\User::all();

        //send to view using compact method
        return view('backend.user.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['roles'] = Role::pluck('name','id');
        return view('backend.user.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        //add login user id to request object
        // $request->request->add(['created_by' =>Auth::user()->id]);
        $password = Hash::make('your password');
        $request->request->add(['created_by' =>Auth::user()->id]);
        //dd($request->all());

        $user = \App\Model\User::create($request->all());
        if ($user){
            $request->session()->flash('success_message','User Created Successfully');
            return redirect()->route('user.index');
        }else{
            $request->session()->flash('error_message','User Creation failed');
            return redirect()->route('user.create');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //single data sho only 'tag ' written
        $data['users'] = \App\Model\User::find($id);
        $data['user']=User::where('id',$id)->get();
        return view('backend.user.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['roles'] = Role::pluck('name','id');
        //single data sho only 'tag ' written
        //cpact for passing data
        $data['users'] = \App\Model\User::find($id);
        return view('backend.user.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());
        //add login user id to request object
        // $request->request->add(['created_by' =>Auth::user()->id]);
        $request->request->add(['updated_by' =>Auth::user()->id]);
        //dd($request->all());
        //select data from tag table using tag model

        $user = \App\Model\User::find($id);
        if ($user->update($request->all())){
            $request->session()->flash('success_message','User update Successfully');

        }else{
            $request->session()->flash('error_message','User Update failed');

        }
        return redirect()->route('user.index');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $user = User::find($id);
        if ($user->delete()){
            $request->session()->flash('success_message','User deleted Successfully');

        }else{
            $request->session()->flash('error_message','User deleted failed');

        }
        return redirect()->route('user.index');

    }
    }

