<div class="form-group">
    <label for="role_id">Role id</label>
    {!! Form::select('role_id', $data['roles'],null); !!}

</div>
<div class="form-group">

        {!!  Form::label('name', 'Name'); !!}
        {!! Form::text('name', null,['class' => 'form-control','id' => 'name','placeholder' => 'enter name']); !!}
        @include('includes.single_field_validation',['field'=>'name'])

    @include('includes.single_field_validation',['field'=>'name'])
</div>
<div class="form-group">

    {!!  Form::label('email', 'Email'); !!}
    {!! Form::text('email', null,['class' => 'form-control','id' => 'email','placeholder' => 'enter email']); !!}
    @include('includes.single_field_validation',['field'=>'email'])

    @include('includes.single_field_validation',['field'=>'email'])
</div>
<div class="form-group">
    {!!  Form::label('password', 'Password'); !!}
    {!! Form::text('password', null,['class' => 'form-control','id' => 'password','placeholder' => 'enter password']); !!}
        @include('includes.single_field_validation',['field'=>'password'])

</div>
