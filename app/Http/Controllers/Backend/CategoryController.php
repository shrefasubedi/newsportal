<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\CategoryRequest;
use App\Model\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['categories'] = Category::all();
        return view('backend.category.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        if (!empty($request->file('photo'))) {

            $category_image = $request->file('photo');

            $image_name = uniqid() . '.' . $category_image->getClientOriginalExtension();
            $destinationPath = public_path('/images/categories');
            $category_image->move($destinationPath, $image_name);
            $request->request->add(['feature_image' => $image_name]);
        }

        //add extra column
        $request->request->add(['created_by' => Auth::user()->id]);
        //save data
        $category = Category::create($request->all());
        if ($category){
            $request->session()->flash('success_message','Category Insert Success!!');
            return redirect()->route('category.index');
        }else{
            $request->session()->flash('error_message','Category Insert Failed!!');
            return redirect()->route('category.create');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['categories']  = Category::find($id);
        $data['category']=Category::where('id',$id)->get();
        return view('backend.category.show',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['category'] = Category::find($id);
        return view('backend.category.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!empty($request->file('photo'))) {

            $category_image = $request->file('photo');

            $image_name = uniqid() . '.' . $category_image->getClientOriginalExtension();
            $destinationPath = public_path('/images/categories');
            $category_image->move($destinationPath, $image_name);
            $request->request->add(['photo' => $image_name]);
        }




        $category = Category::find($id);
        $request->request->add(['updated_by' => Auth::user()->id]);
        //save data
        $category = $category->update($request->all());
        if ($category){
            $request->session()->flash('success_message','Category Update Success!!');

        }else{
            $request->session()->flash('error_message','Category Update Failed!!');

        }
        return redirect()->route('category.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request ,$id)
    {
        $category = Category::find($id);

        if ($category->delete()){
            $request->session()->flash('success_message','Category Delete Success!!');

        }else{
            $request->session()->flash('error_message','Category Delete Failed!!');

        }
        return redirect()->route('category.index');
    }

    function news(){
        $category= Category::find($_POST['cid']);
        $ht ="<option value=''>Select news</option>";
        foreach ($category->news as $news){
            $ht .="<option value='$news->id'>$news->name</option>";
        }
        return $ht;

    }

    function delete_image(Request $request, $id){
        $category = Category::find($id);


        if ($category->delete()){
            $request->session()->flash('success_message','Category Delete Success!!');
        }else{
            $request->session()->flash('error_message','Category Delete Failed!!');
        }
        return redirect()->route('category.index');
    }

}
