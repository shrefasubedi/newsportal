<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table ='news';

    protected  $fillable = ['category_id','author_id','title','slug','feature_image','status','meta_keyword','meta_description','short_description','description','created_by','updated_by'];


    public function tags(){
        return $this->belongsToMany(Tag::class, 'news_tag','news_id');
    }
    public function newstypes(){
        return $this->belongsToMany(NewsType::class,'news_newstype','news_id');
    }
    public function category(){
        return $this->belongsTo(Category::class);
    }
    public function author(){
        return $this->belongsTo(Author::class);
    }

    public function comments(){
            return $this->hasMany(Comment::class);
    }
}
