<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



//Frontend
Route::prefix('/')->namespace('Frontend')->group(function () {
    Route::get('', 'FrontController@index')->name('frontend.index');

    Route::get('category/{slug}', 'FrontController@category')->name('frontend.category');
    Route::get('categorydetail/{slug}', 'FrontController@categorydetail')->name('frontend.categorydetail');
    Route::get('postdetail/{slug}', 'FrontController@postdetail')->name('frontend.postdetail');
    Route::get('contact', 'FrontController@contact')->name('frontend.contact');
    Route::post('contact', 'FrontController@store')->name('frontend.store');

    Route::get('news/{slug}', 'FrontController@news')->name('frontend.news');
    Route::get('popular_news/{slug}', 'FrontController@popular_news')->name('frontend.popular_news');
    Route::get('top_news/{slug}', 'FrontController@top_news')->name('frontend.top_news');
    Route::get('/search', 'FrontController@search')->name('frontend.search');
//    Route::get('news1/{slug}', 'FrontController@news1')->name('frontend.news1');

    Route::post('comment', 'FrontController@comment')->name('frontend.comment');
});








//Backend
Route::prefix('backend/')->namespace('Backend')->group(function () {
    //create index

//create index
    Route::get('category', 'CategoryController@index')->name('category.index');
//create form
    Route::get('category/create', 'CategoryController@create')->name('category.create');
//store into database
    Route::post('category', 'CategoryController@store')->name('category.store');
//view details
    Route::get('category/{id}', 'CategoryController@show')->name('category.show');
//edit form
    Route::get('category/{id}/edit', 'CategoryController@edit')->name('category.edit');
//update into database
    Route::put('category/{id}', 'CategoryController@update')->name('category.update');
//delete form database
    Route::delete('category/{id}', 'CategoryController@destroy')->name('category.destroy');
    //delete category image form database
    Route::delete('category/{id}/delete_image', 'CategoryController@delete_image')->name('category.delete_image');

    //create index
    Route::get('video', 'VideoController@index')->name('video.index');
//create form
    Route::get('video/create', 'VideoController@create')->name('video.create');
//store into database
    Route::post('video', 'VideoController@store')->name('video.store');
//view details
    Route::get('video/{id}', 'VideoController@show')->name('video.show');
//edit form
    Route::get('video/{id}/edit', 'VideoController@edit')->name('video.edit');
//update into database
    Route::put('video/{id}', 'VideoController@update')->name('video.update');
//delete form database
    Route::delete('video/{id}', 'VideoController@destroy')->name('video.destroy');


    //create index(news)
    Route::get('news', 'NewsController@index')->name('news.index');
//create form
    Route::get('news/create', 'NewsController@create')->name('news.create');
//store into database
    Route::post('news', 'NewsController@store')->name('news.store');
//view details
    Route::get('news/{id}', 'NewsController@show')->name('news.show');
//edit form
    Route::get('news/{id}/edit', 'NewsController@edit')->name('news.edit');
//update into database
    Route::put('news/{id}', 'NewsController@update')->name('news.update');
//delete form database
    Route::delete('news/{id}', 'NewsController@destroy')->name('news.destroy');
    Route::get('/search', 'NewsController@search')->name('news.search');
    //get subcategory
    Route::post('category/news', 'CategoryController@subcategory')->name('category.news');

    //create index(comment)
    Route::get('comment', 'CommentController@index')->name('comment.index');
//create form
    Route::get('comment/create', 'CommentController@create')->name('comment.create');
//store into database
    Route::post('comment', 'CommentController@store')->name('comment.store');
//view details
    Route::get('comment/{id}', 'CommentController@show')->name('comment.show');
//edit form
    Route::get('comment/{id}/edit', 'CommentController@edit')->name('comment.edit');
//update into database
    Route::put('comment/{id}', 'CommentController@update')->name('comment.update');
//delete form database
    Route::delete('comment/{id}', 'CommentController@destroy')->name('comment.destroy');

    //create index(comment)
    Route::get('page', 'PageController@index')->name('page.index');
//create form
    Route::get('page/create', 'PageController@create')->name('page.create');
//store into database
    Route::post('page', 'PageController@store')->name('page.store');
//view details
    Route::get('page/{id}', 'PageController@show')->name('page.show');
//edit form
    Route::get('page/{id}/edit', 'PageController@edit')->name('page.edit');
//update into database
    Route::put('page/{id}', 'PageController@update')->name('page.update');
//delete form database
    Route::delete('page/{id}', 'PageController@destroy')->name('page.destroy');


    //create index(newstype)
    Route::get('newstype', 'NewsTypeController@index')->name('newstype.index');
//create form
    Route::get('newstype/create', 'NewsTypeController@create')->name('newstype.create');
//store into database
    Route::post('newstype', 'NewsTypeController@store')->name('newstype.store');
//view details
    Route::get('newstype/{id}', 'NewsTypeController@show')->name('newstype.show');
//edit form
    Route::get('newstype/{id}/edit', 'NewsTypeController@edit')->name('newstype.edit');
//update into database
    Route::put('newstype/{id}', 'NewsTypeController@update')->name('newstype.update');
//delete form database
    Route::delete('newstype/{id}', 'NewsTypeController@destroy')->name('newstype.destroy');


    //slider
    //create index
    Route::get('news_slider', 'NewsSliderController@index')->name('news_slider.index');
//create form
    Route::get('news_slider/create', 'NewsSliderController@create')->name('news_slider.create');
//store into database
    Route::post('news_slider', 'NewsSliderController@store')->name('news_slider.store');
//view details
    Route::get('news_slider/{id}', 'NewsSliderController@show')->name('news_slider.show');
//edit form
    Route::get('news_slider/{id}/edit', 'NewsSliderController@edit')->name('news_slider.edit');
//update into database
    Route::put('news_slider/{id}', 'NewsSliderController@update')->name('news_slider.update');
//delete form database
    Route::delete('news_slider/{id}', 'NewsSliderController@destroy')->name('news_slider.destroy');

    //index
    Route::get('tag', 'TagController@index')->name('tag.index');
//create form
    Route::get('tag/create', 'TagController@create')->name('tag.create');
//store into database
    Route::post('tag', 'TagController@store')->name('tag.store');
//view details
    Route::get('tag/{id}', 'TagController@show')->name('tag.show');
//edit form
    Route::get('tag/{id}/edit', 'TagController@edit')->name('tag.edit');
//update into database
    Route::put('tag/{id}', 'TagController@update')->name('tag.update');
//delete form database
    Route::delete('tag/{id}', 'TagController@destroy')->name('tag.destroy');


    //index
    Route::get('advertisement', 'AdvertisementController@index')->name('advertisement.index');
//create form
    Route::get('advertisement/create', 'AdvertisementController@create')->name('advertisement.create');
//store into database
    Route::post('advertisement', 'AdvertisementController@store')->name('advertisement.store');
//view details
    Route::get('advertisement/{id}', 'AdvertisementController@show')->name('advertisement.show');
//edit form
    Route::get('advertisement/{id}/edit', 'AdvertisementController@edit')->name('advertisement.edit');
//update into database
    Route::put('advertisement/{id}', 'AdvertisementController@update')->name('advertisement.update');
//delete form database
    Route::delete('advertisement/{id}', 'AdvertisementController@destroy')->name('advertisement.destroy');


    //index gallery
    Route::get('gallery', 'GalleryController@index')->name('gallery.index');
//create form
    Route::get('gallery/create', 'GalleryController@create')->name('gallery.create');
//store into database
    Route::post('gallery', 'GalleryController@store')->name('gallery.store');
//view details
    Route::get('gallery/{id}', 'GalleryController@show')->name('gallery.show');
//edit form
    Route::get('gallery/{id}/edit', 'GalleryController@edit')->name('gallery.edit');
//update into database
    Route::put('gallery/{id}', 'GalleryController@update')->name('gallery.update');
//delete form database
    Route::delete('gallery/{id}', 'GalleryController@destroy')->name('gallery.destroy');


    //role
    //create index
    Route::get('role', 'RoleController@index')->name('role.index');
//create form
    Route::get('role/create', 'RoleController@create')->name('role.create');
//store into database
    Route::post('role', 'RoleController@store')->name('role.store');
//view details
    Route::get('role/{id}', 'RoleController@show')->name('role.show');
//edit form
    Route::get('role/{id}/edit', 'RoleController@edit')->name('role.edit');
//update into database
    Route::put('role/{id}', 'RoleController@update')->name('role.update');
//delete form database
    Route::delete('role/{id}', 'RoleController@destroy')->name('role.destroy');

    //role
    Route::get('role/assignpermission/{id}', 'RoleController@assignPermission')->name('role.assignpermission');
    Route::post('role/savepermission/{id}', 'RoleController@savePermission')->name('role.savepermission');

    //create module
    Route::get('module', 'ModuleController@index')->name('module.index');
//create form
    Route::get('module/create', 'ModuleController@create')->name('module.create');
//store into database
    Route::post('module', 'ModuleController@store')->name('module.store');
//view details
    Route::get('module/{id}', 'ModuleController@show')->name('module.show');
//edit form
    Route::get('module/{id}/edit', 'ModuleController@edit')->name('module.edit');
//update into database
    Route::put('module/{id}', 'ModuleController@update')->name('module.update');
//delete form database
    Route::delete('module/{id}', 'ModuleController@destroy')->name('module.destroy');

//role
    //create index
    Route::get('permission', 'PermissionController@index')->name('permission.index');
//create form
    Route::get('permission/create', 'PermissionController@create')->name('permission.create');
//store into database
    Route::post('permission', 'PermissionController@store')->name('permission.store');
//view details
    Route::get('permission/{id}', 'PermissionController@show')->name('permission.show');
//edit form
    Route::get('permission/{id}/edit', 'PermissionController@edit')->name('permission.edit');
//update into database
    Route::put('permission/{id}', 'PermissionController@update')->name('permission.update');
//delete form database
    Route::delete('permission/{id}', 'PermissionController@destroy')->name('permission.destroy');

   // create index
    Route::get('author', 'AuthorController@index')->name('author.index');
//create form
    Route::get('author/create', 'AuthorController@create')->name('author.create');
//store into database
    Route::post('author', 'AuthorController@store')->name('author.store');
//view details
    Route::get('author/{id}', 'AuthorController@show')->name('author.show');
//edit form
    Route::get('author/{id}/edit', 'AuthorController@edit')->name('author.edit');
//update into database
    Route::put('author/{id}', 'AuthorController@update')->name('author.update');
//delete form database
    Route::delete('author/{id}', 'AuthorController@destroy')->name('author.destroy');


//user table
    //create index
    Route::get('user', 'UserController@index')->name('user.index');
//create form
    Route::get('user/create', 'UserController@create')->name('user.create');
//store into database
    Route::post('user', 'UserController@store')->name('user.store');
//view details
    Route::get('user/{id}', 'UserController@show')->name('user.show');
//edit form
    Route::get('user/{id}/edit', 'UserController@edit')->name('user.edit');
//update into database
    Route::put('user/{id}', 'UserController@update')->name('user.update');
//delete form database
    Route::delete('user/{id}', 'UserController@destroy')->name('user.destroy');

    //configuration
    Route::get('configuration', 'ConfigurationController@index')->name('configuration.index');
//create form
    Route::get('configuration/create', 'ConfigurationController@create')->name('configuration.create');
//store into database
    Route::post('configuration', 'ConfigurationController@store')->name('configuration.store');
//view details
    Route::get('configuration/{id}', 'ConfigurationController@show')->name('configuration.show');
//edit form
    Route::get('configuration/{id}/edit', 'ConfigurationController@edit')->name('configuration.edit');
//update into database
    Route::put('configuration/{id}', 'ConfigurationController@update')->name('configuration.update');
//delete form database
    Route::delete('configuration/{id}', 'ConfigurationController@destroy')->name('configuration.destroy');

});

Route::get('/profile', 'HomeController@profile')->name('backend.profile');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
