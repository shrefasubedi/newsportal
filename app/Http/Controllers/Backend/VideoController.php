<?php

namespace App\Http\Controllers\Backend;

use App\Model\Video;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data['videos'] = Video::all();
        return view('backend.video.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.video.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->request->add(['created_by' =>Auth::user()->id]);
        //dd($request->all());

        $video = Video::create($request->all());
        if ($video){
            $request->session()->flash('success_message','Tag Created Successfully');
            return redirect()->route('video.index');
        }else{
            $request->session()->flash('error_message','Tag Creation failed');
            return redirect()->route('video.create');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['videos'] = Tag::find($id);
        return view('backend.video.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['videos'] = Video::find($id);
        return view('backend.video.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());
        //add login user id to request object
        // $request->request->add(['created_by' =>Auth::user()->id]);
        $request->request->add(['updated_by' =>Auth::user()->id]);
        //dd($request->all());
        //select data from tag table using tag model

        $video = Video::find($id);
        if ($video->update($request->all())){
            $request->session()->flash('success_message','Video update Successfully');

        }else{
            $request->session()->flash('error_message','Video Update failed');

        }
        return redirect()->route('video.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $video = Video::find($id);
        if ($video->delete()){
            $request->session()->flash('success_message','Video deleted Successfully');

        }else{
            $request->session()->flash('error_message','Video deleted failed');

        }
        return redirect()->route('video.index');
    }
}
