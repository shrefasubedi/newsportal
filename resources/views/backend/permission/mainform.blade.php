<div class="form-group">
    <label for="module_id">Module id</label>
    {!! Form::select('module_id', $data['modules'],null); !!}

</div>
<div class="form-group">

        {!!  Form::label('name', 'Name'); !!}
        {!! Form::text('name', null,['class' => 'form-control','id' => 'name','placeholder' => 'enter name']); !!}
        @include('includes.single_field_validation',['field'=>'name'])

    @include('includes.single_field_validation',['field'=>'name'])
</div>
<div class="form-group">
    {!!  Form::label('route', 'Route'); !!}
    {!! Form::text('route', null,['class' => 'form-control','id' => 'route','placeholder' => 'enter route']); !!}
        @include('includes.single_field_validation',['field'=>'route'])

</div>
<div class="form-group">
    {!!  Form::label('status', 'Status'); !!}
    {!! Form::radio('status', '1') !!} Active
    {!! Form::radio('status', '0',true) !!} De Active
</div>