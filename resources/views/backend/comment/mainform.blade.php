<div class="form-group">
    <label for="news_id">News id</label>
    {!! Form::select('news_id', $data['news'],null); !!}

</div>

<div class="form-group">
    {!!  Form::label('name', 'Name'); !!}
    {!! Form::text('name', null,['class' => 'form-control','id' => 'name','placeholder' => 'enter name']); !!}
    @include('includes.single_field_validation',['field'=>'name'])
</div>

<div class="form-group">
    {!!  Form::label('email', 'Email'); !!}
    {!! Form::text('email', null,['class' => 'form-control','id' => 'email']); !!}

    @include('includes.single_field_validation',['field'=>'email'])
</div>
<div class="form-group">
        {!!  Form::label('comment', 'Comment'); !!}
    {!! Form::text('comment', null,['class' => 'form-control','id' => 'comment']); !!}

    @include('includes.single_field_validation',['field'=>'comment'])
</div>

<div class="form-group">
    {!!  Form::label('status', 'Status'); !!}
    {!! Form::radio('status', '1') !!} Active
    {!! Form::radio('status', '0',true) !!} De Active
</div>