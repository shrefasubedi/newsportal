<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    protected $table ='authors';

    protected  $fillable = ['name','delegation','image','status'];

    public function news(){
        return $this->belongsTo(News::class);
    }
}
