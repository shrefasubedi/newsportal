<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\NewsTypeRequest;
use App\Model\NewsType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class NewsTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['newstype'] = NewsType::all();
        return view('backend.newstype.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('backend.newstype.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NewsTypeRequest $request)
    {
        if (!empty($request->file('photo'))) {

            $newstype_image = $request->file('photo');

            $image_name = uniqid() . '.' . $newstype_image->getClientOriginalExtension();
            $destinationPath = public_path('/images/newstypes');
            $newstype_image->move($destinationPath, $image_name);
            $request->request->add(['feature_image' => $image_name]);
        }
        //add extra column
        $request->request->add(['created_by' => Auth::user()->id]);
        //save data
        $newstype = NewsType::create($request->all());
        if ($newstype){
            $request->session()->flash('success_message','NewsType Insert Success!!');
            return redirect()->route('newstype.index');
        }else{
            $request->session()->flash('error_message','NewsType Insert Failed!!');
            return redirect()->route('newstype.create');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['newstype']  = NewsType::find($id);
        $data['newstypes']=NewsType::where('id',$id)->get();
        return view('backend.newstype.show',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['newstype'] = NewsType::find($id);
        return view('backend.newstype.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(NewsTypeRequest $request, $id)
    {
        if (!empty($request->file('photo'))) {

            $newstype_image = $request->file('photo');

            $image_name = uniqid() . '.' . $newstype_image->getClientOriginalExtension();
            $destinationPath = public_path('/images/newstypes');
            $newstype_image->move($destinationPath, $image_name);
            $request->request->add(['feature_image' => $image_name]);
        }


        $newstype = NewsType::find($id);
        $request->request->add(['updated_by' => Auth::user()->id]);
        //save data
        $newstype = $newstype->update($request->all());
        if ($newstype){
            $request->session()->flash('success_message','NewsType Update Success!!');

        }else{
            $request->session()->flash('error_message','NewsType Update Failed!!');

        }
        return redirect()->route('newstype.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $newstype = NewsType::find($id);

        if ($newstype->delete()){
            $request->session()->flash('success_message','NewsType Delete Success!!');

        }else{
            $request->session()->flash('error_message','NewsType Delete Failed!!');

        }
        return redirect()->route('newstype.index');
    }

}
