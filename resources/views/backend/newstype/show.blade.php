@extends('layouts.backend')
@section('title','NewsType Show page')

@section('content')
    <section class="content-header">
        <h1>
                NewsType Management
            <a href="{{route('newstype.create')}}" class="btn btn-success">
                <i class="fa fa-plus"></i>
                Create
            </a>
            <a href="{{route('newstype.index')}}" class="btn btn-success">
                <i class="fa fa-plus"></i>
                List
            </a>

        </h1>
        <ol class="breadcrumb">
            <li><a href="{{route('home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('newstype.index')}}">NewsType</a></li>
            <li class="active">Show page</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Show page</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                @include('includes.flash')
               <table class="table table-bordered">
                   <thead>
                   <tr>
                       <th>Name</th>
                       <td>{{$data['newstype']->name}}</td>

                   </tr>

                   <tr>
                       <th>Status</th>
                       <td>
                           @if($data['newstype']->status==1)
                               <span class="label label-success">Active</span>
                           @else
                               <span class="label label-danger">Deactive</span>
                               @endif
                       </td>
                   </tr>
                   <tr>
                       <th>Slug</th>
                       <td>{{$data['newstype']->slug}}</td>
                   </tr>
                   <tr>
                       <th>Feature Image</th>
                       <td>
                           <img src="{{asset("images/newstypes/".$data['newstype']->image)}}" alt=""></td>
                   </tr>
                   <tr>

                   <tr>
                       <th>Created By</th>
                       <td>{{App\User::find($data['newstype']->created_by)->name}}</td>
                   </tr>
                   <tr>
                       <th>Created At</th>
                       <td></td>
                   </tr>
                   <tr>
                       <th>Updated At</th>
                       <td></td>
                   </tr>
                   <tr>
                       <th>Updated By</th>
                       <td>@if(!empty($data['newstype']->updated_by))</td>
                       {{App\User::find($data['newstype']->updated_by)->name}}
                       @endif
                   </tr>
                   <tr>
                       <th>Action</th>
                       @foreach($data['newstypes'] as $newstype)
                           <td>
                               <a href="{{route('newstype.edit',$newstype->id)}}" class="btn btn-warning">
                                   <i class="fa fa-pencil"></i>
                                   Edit
                               </a>
                               <form action="{{route('newstype.destroy',$newstype->id)}}" method="post"
                                     onsubmit="return confirm('Are you sure?')">
                                   @csrf
                                   <input type="hidden" name="_method" value="DELETE"/>
                                   <button class="btn-danger"><i class="fa fa-trash"></i>Delete</button>
                               </form>
                           </td>
                       @endforeach
                   </tr>
                   </thead>
               </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                Footer
            </div>
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection