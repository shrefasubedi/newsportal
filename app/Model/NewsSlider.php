<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class NewsSlider extends Model
{
    protected $table ='news_sliders';

    protected  $fillable = ['title','slug','comment','image','link','status','meta_keyword','meta_description','short_description','description','created_by','updated_by'];

}
