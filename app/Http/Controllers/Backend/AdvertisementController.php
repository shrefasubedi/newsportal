<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\AdvertisementRequest;
use App\Model\Advertisement;
use App\Model\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdvertisementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['advertisements']= Advertisement::all();

        return view('backend.advertisement.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['pages'] = Page::pluck('page_name','id');

        return view('backend.advertisement.create',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdvertisementRequest $request)
    {
        if (!empty($request->file('photo'))) {

            $advertisement_image = $request->file('photo');

            $image_name = uniqid() . '.' . $advertisement_image->getClientOriginalExtension();
            $destinationPath = public_path('/images/advertisements');
            $advertisement_image->move($destinationPath, $image_name);
            $request->request->add(['image' => $image_name]);
        }
        //add extra column
        $request->request->add(['created_by' => Auth::user()->id]);
        //save data
        $advertisement = Advertisement::create($request->all());
        if ($advertisement){
            $request->session()->flash('success_message','Advertisement Insert Success!!');
            return redirect()->route('advertisement.index');
        }else{
            $request->session()->flash('error_message','Advertisement Insert Failed!!');
            return redirect()->route('advertisement.create');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['advertisements']  = Advertisement::find($id);
        $data['advertisement']  = Advertisement::where('id',$id)->get();
        return view('backend.advertisement.show',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['pages'] = Page::pluck('page_name','id');
        $data['advertisement'] = Advertisement::find($id);
        return view('backend.advertisement.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!empty($request->file('photo'))) {

            $advertisement_image = $request->file('photo');

            $image_name = uniqid() . '.' . $advertisement_image->getClientOriginalExtension();
            $destinationPath = public_path('/images/advertisements');
            $advertisement_image->move($destinationPath, $image_name);
            $request->request->add(['image' => $image_name]);
        }


        $advertisement = Advertisement::find($id);
        $request->request->add(['updated_by' => Auth::user()->id]);
        //save data
        $advertisement = $advertisement->update($request->all());
        if ($advertisement){
            $request->session()->flash('success_message','Advertisement Update Success!!');

        }else{
            $request->session()->flash('error_message','Advertisement Update Failed!!');

        }
        return redirect()->route('advertisement.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $advertisement = Advertisement::find($id);

        if ($advertisement->delete()){
            $request->session()->flash('success_message','Advertisement Delete Success!!');

        }else{
            $request->session()->flash('error_message','Advertisement Delete Failed!!');

        }
        return redirect()->route('advertisement.index');
    }
}
