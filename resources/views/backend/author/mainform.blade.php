<div class="form-group">
    {!!  Form::label('name', 'Name'); !!}
    {!! Form::text('name', null,['class' => 'form-control','id' => 'name','placeholder' => 'enter name']); !!}
    @include('includes.single_field_validation',['field'=>'name'])
</div>
<div class="form-group">
    {!!  Form::label('delegation', 'Delegation'); !!}
    {!! Form::text('delegation', null,['class' => 'form-control','id' => 'delegation']); !!}

    @include('includes.single_field_validation',['field'=>'delegation'])
</div>

<div class="form-group">
    {!!  Form::label('photo', ' Image'); !!}
    {!! Form::file('photo'); !!}

    @include('includes.single_field_validation',['field'=>'photo'])
</div>

<div class="form-group">
    {!!  Form::label('status', 'Status'); !!}
    {!! Form::radio('status', '1') !!} Active
    {!! Form::radio('status', '0',true) !!} De Active
</div>