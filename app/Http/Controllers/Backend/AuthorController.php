<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\AuthorRequest;
use App\Model\Author;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AuthorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['authors'] = Author::all();
        return view('backend.author.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.author.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AuthorRequest $request)
    {
        if (!empty($request->file('photo'))) {

            $author_image = $request->file('photo');

            $image_name = uniqid() . '.' . $author_image->getClientOriginalExtension();
            $destinationPath = public_path('/images/authors');
            $author_image->move($destinationPath, $image_name);
            $request->request->add(['image' => $image_name]);
        }

        //add extra column
        $request->request->add(['created_by' => Auth::user()->id]);
        //save data
        $author = Author::create($request->all());
        if ($author){
            $request->session()->flash('success_message','Author Insert Success!!');
            return redirect()->route('author.index');
        }else{
            $request->session()->flash('error_message','Author Insert Failed!!');
            return redirect()->route('author.create');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['authors']  = Author::find($id);
        return view('backend.author.show',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['authors'] = Author::find($id);
        return view('backend.author.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!empty($request->file('photo'))) {

            $author_image = $request->file('photo');

            $image_name = uniqid() . '.' . $author_image->getClientOriginalExtension();
            $destinationPath = public_path('/images/authors');
            $author_image->move($destinationPath, $image_name);
            $request->request->add(['image' => $image_name]);
        }




        $author = Author::find($id);
        $request->request->add(['updated_by' => Auth::user()->id]);
        //save data
        $author = $author->update($request->all());
        if ($author){
            $request->session()->flash('success_message','Author Update Success!!');

        }else{
            $request->session()->flash('error_message','Author Update Failed!!');

        }
        return redirect()->route('author.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request , $id)
    {
        $author = Author::find($id);

        if ($author->delete()){
            $request->session()->flash('success_message','Author Delete Success!!');

        }else{
            $request->session()->flash('error_message','Author Delete Failed!!');

        }
        return redirect()->route('author.index');
    }

    function news(){
        $author= Author::find($_POST['cid']);
        $ht ="<option value=''>Select author</option>";
        foreach ($author->news as $news){
            $ht .="<option value='$news->id'>$news->name</option>";
        }
        return $ht;

    }

    function delete_image(Request $request, $id){
        $author = Author::find($id);


        if ($author->delete()){
            $request->session()->flash('success_message','Author Delete Success!!');
        }else{
            $request->session()->flash('error_message','Author Delete Failed!!');
        }
        return redirect()->route('author.index');
    }

}
