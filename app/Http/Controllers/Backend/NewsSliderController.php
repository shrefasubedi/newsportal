<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\NewsSliderRequest;
use App\Model\NewsSlider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class NewsSliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['news_sliders'] = NewsSlider::all();
        return view('backend.news_slider.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.news_slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //add extra column
        $request->request->add(['created_by' => Auth::user()->id]);
        //save data

        $slider_image = $request->file('photo');

        $image_name =  uniqid() . '.' . $slider_image->getClientOriginalExtension();
        $destinationPath = public_path('/images/news_sliders');
        $slider_image->move($destinationPath, $image_name);
        $request->request->add(['image' =>$image_name]);


        $news_slider = NewsSlider::create($request->all());
        if ($news_slider){
            $request->session()->flash('success_message','NewsSlider Insert Success!!');
            return redirect()->route('news_slider.index');
        }else{
            $request->session()->flash('error_message','NewsSlider Insert Failed!!');
            return redirect()->route('news_slider.create');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['news_sliders']  = NewsSlider::find($id);
            $data['news_slider']=NewsSlider::where('id',$id)->get();
        return view('backend.news_slider.show',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['news_sliders']=NewsSlider::find($id);
        return view('backend.news_slider.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(NewsSliderRequest $request, $id)
    {
        $request->request->add(['updated_by' =>Auth::user()->id]);

        if ($request->file('photo')) {
            $news_slider_image = $request->file('photo');

            $image_name = uniqid() . '.' . $news_slider_image->getClientOriginalExtension();
            $destinationPath = public_path('/images/news_sliders');
            $news_slider_image->move($destinationPath, $image_name);
            $request->request->add(['image' => $image_name]);
        }
        //insert into tag table using Tag model
        $news_slider = NewsSlider::find($id);
        if ($news_slider->update($request->all())) {
            $request->session()->flash('success_message', 'NewsSlider Updated Successfully');

        } else {
            $request->session()->flash('error_message', 'NewsSlider Updated Failed');

        }
        return redirect()->route('news_slider.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $news_slider= NewsSlider::find($id);
        if ($news_slider->delete()) {
            $request->session()->flash('success_message', 'Slider Deleted Successfully');

        } else {
            $request->session()->flash('error_message', 'Slider Deleted Failed');

        }
        return redirect()->route('news_slider.index');
    }
}
